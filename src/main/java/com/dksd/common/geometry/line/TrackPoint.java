package com.cycling.science.trainwithscience;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dscottdawkins on 5/20/17.
 */

public class TrackPoint {

    private double latitude;
    private int latCount;
    private double longitude;
    private int lonCount;
    private String time;
    private double power;
    private int pwrCount;
    private double cadence;
    private int cadenceCount;
    private double hr;
    private int hrCount;
    private double alt;
    private int altCount;

    public void setAlt(double alt) {
        this.alt += alt;
        this.altCount++;
    }

    public void setLat(double lat) {
        this.latitude += lat;
        this.latCount++;
    }

    public void setLon(double lon) {
        this.longitude += lon;
        lonCount++;
    }

    public void setPower(double power) {
        this.power += power;
        this.pwrCount++;
    }

    public void setCadence(double cadence) {
        this.cadence += cadence;
        cadenceCount++;
    }

    public void setHr(double hr) {
        this.hr = hr; hrCount++;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getLines() {
        List<String> ret = new ArrayList<>(14);
        ret.add("   <trkpt lat=\""+getLat()+"\" lon=\""+getLon()+"\">");
        ret.add("    <ele>"+getAlt()+"</ele>");
        ret.add("    <time>"+time+"</time>");
        ret.add("    <extensions>");
        ret.add("     <gpxtpx:TrackPointExtension>");
        if (Math.abs(getHr()) > 30)
        ret.add("      <gpxtpx:hr>"+getHr()+"</gpxtpx:hr>");
        if (Math.abs(getCadence()) > 5)
        ret.add("      <gpxtpx:cad>"+getCadence()+"</gpxtpx:cad>");
        ret.add("     </gpxtpx:TrackPointExtension>");
        if (Math.abs(getPower()) > 1)
        ret.add("     <power>"+getPower()+"</power>");
        ret.add("    </extensions>");
        ret.add("   </trkpt>");
        return ret;
    }

    public void addDataPoint(DataField field, double value) {
        switch (field) {
            case POWER: setPower(value);break;
            case LATITUDE: setLat(value);break;
            case LONGITUDE: setLon(value);break;
            case ALTITUDE: setAlt(value);break;
            case HR_HEART_RATE: setHr(value);break;
            case CADENCE_CALC: setCadence(value); break;
            case PWR_CADENCE: setCadence(value); break;
            //case PWR_CALC_POWER: setCadence(value);
            default: break;
        }
    }

    public double getLat() {
        return avg(latitude, latCount);
    }

    public double getLon() {
        return avg(longitude,lonCount);
    }

    public String getTime() {
        return time;
    }

    public double getPower() {
        return avg(power,pwrCount);
    }

    public double getAlt() {
        return avg(alt,altCount);
    }

    public double getCadence() {
        return avg(cadence, cadenceCount);
    }

    public double getHr() {
        return avg(hr, hrCount);
    }

    private double avg(double val, int count) {
        if (count == 0) {
            return 0;
        }
        return val/(double)count;
    }

}
