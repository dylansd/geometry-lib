package co.uk.ptl.indicators;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import co.uk.ptl.common.PeriodProperties;
import co.uk.ptl.evaluation.MarketEvaluator;
import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.turningpoint.TurningPointVOImpl;
import co.uk.ptl.vos.FibPattern;
import co.uk.ptl.vos.FibPatternImpl;
import co.uk.ptl.vos.ParseDataVO;
import co.uk.ptl.vos.ProfitLevel;

public class AbcdOptimizerImpl implements AbcdOptimizer {

   private List<FibPattern> fibPatterns = new ArrayList<FibPattern>();

   public AbcdOptimizerImpl() {
      generateFibPatterns();
   }

   private void generateFibPatterns() {
      // 0%, 23.6%, 38.2%, 50%, 61.8%, 76.4%, 100%}.
      fibPatterns.add(new FibPatternImpl(76.4, 23.6));
      fibPatterns.add(new FibPatternImpl(76.4, 38.2));
      //fibPatterns.add(new FibPatternImpl(76.4, 50));
      //fibPatterns.add(new FibPatternImpl(76.4, 61.8));
      fibPatterns.add(new FibPatternImpl(61.8, 23.6));
      fibPatterns.add(new FibPatternImpl(61.8, 38.2));
      //fibPatterns.add(new FibPatternImpl(61.8, 50));
      //fibPatterns.add(new FibPatternImpl(50, 38.2));
      //fibPatterns.add(new FibPatternImpl(50, 23.6));

   }

   public List<ProfitLevel> getOptimalAbcd(Vector<TurningPointVO> turningPoints, MarketEvaluator statObj, Vector<ParseDataVO> vos,
      ParseDataVO latestVo, PeriodProperties prop) {
      if (vos == null) {
         return null;
      }
      if (vos.size() - 1 - prop.getPastWindow() < 0) {
         return null;
      }
      List<ProfitLevel> profitLevels = new ArrayList<ProfitLevel>();
      TurningPointVO highB = null;
      TurningPointVO lowA = null;
      for (TurningPointVO tp : turningPoints) {
         if (highB == null) {
            highB = tp;
         }
         if (lowA == null) {
            lowA = tp;
         }
         if (tp.isHighTurningpoint() && tp.getDataVO().getPrice() > highB.getDataVO().getPrice()) {
            highB = tp;
         } else if (!tp.isHighTurningpoint() && tp.getDataVO().getPrice() < lowA.getDataVO().getPrice()) {
            lowA = tp;
         }
      }
      Abcd testAbcd = null;
      if (lowA.getDataVO().getTimestamp() < highB.getDataVO().getTimestamp()) {
         testAbcd = new AbcdImpl(lowA, highB, new TurningPointVOImpl(latestVo, false));
         // 0%, 23.6%, 38.2%, 50%, 61.8%, 76.4%, 100%}.
         for (FibPattern fib : fibPatterns) {
            ProfitLevel pl = testAbcd.getProposedD(fib.getBPercentage(), fib.getCPercentage());
            if (pl != null) {
               if (Math.abs((pl.getFuturePrice() - latestVo.getPrice()) / latestVo.getPrice() * 100) > 0.2) {
                  profitLevels.add(pl);
               }
            }
         }
      } else {
         testAbcd = new AbcdImpl(highB, lowA, new TurningPointVOImpl(latestVo, true));
         // 0%, 23.6%, 38.2%, 50%, 61.8%, 76.4%, 100%}.
         for (FibPattern fib : fibPatterns) {
            ProfitLevel pl = testAbcd.getProposedD(fib.getBPercentage(), fib.getCPercentage());
            if (Math.abs((pl.getFuturePrice() - latestVo.getPrice()) / latestVo.getPrice() * 100) > 0.2) {
               profitLevels.add(pl);
            }
         }
      }
      return profitLevels;
   }

   public void clear() {
      // NOOP		
   }

}
