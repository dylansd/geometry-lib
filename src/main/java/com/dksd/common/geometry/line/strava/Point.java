package com.dksd.cycling.strava.point;

/**
 * The Interface Point.
 */
public interface Point {

	/**
	 * Get the ID of the point
	 * 
	 * @return identity of point
	 */
	int getID();

	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	double getX();

	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	double getY();

	/**
	 * Gets the z.
	 * 
	 * @return the z
	 */
	double getZ();

	/**
	 * Get the distance from this point to another.
	 * 
	 * @param pt
	 *            point of calc
	 * @return double the distance
	 */
	double getDistanceTo(Point pt);
	double getDistanceToQuick(Point pt);

	/**
	 * Get the height between this point and that point
	 * 
	 * @param pt
	 *            target pt
	 * @return double height diff
	 */
	double getHeightTo(Point pt);

	double getBearingTo(Point pt);

}
