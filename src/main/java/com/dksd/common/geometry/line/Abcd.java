package co.uk.ptl.indicators;

import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.vos.ProfitLevel;

public interface Abcd {

   TurningPointVO getA();

   TurningPointVO getB();

   TurningPointVO getC();

   double getAPercent();

   double getBPercent();

   double getCPercent();

   ProfitLevel getProposedD(double estBPercentage, double estCPercentage);

}
