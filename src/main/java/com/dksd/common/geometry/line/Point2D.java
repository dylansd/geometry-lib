package com.dksd.common.geometry.line;

public interface Point2D {

   double getX();

   double getY();

   boolean isFixed();

}
