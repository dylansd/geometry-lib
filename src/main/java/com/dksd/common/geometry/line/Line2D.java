package co.uk.dksd.utils.geometry;

public interface Line2D {

   Point2D getParamCoord(double time);

   Point2D getYCoord(double x);

   Point2D getXCoord(double y);

   double getParamT(Point2D pt); //return t on the line

   Point2D calcIntersect(Line2D line);

   LineEquation getEquation();

}
