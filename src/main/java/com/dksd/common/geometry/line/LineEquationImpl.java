package com.dksd.common.geometry.line;

public class LineEquationImpl implements LineEquation {

   private final double m;
   private final double c;
   private final double a;
   private final double b;
   private final double ct;
   private final double d;

   //(x(t),y(t)) = (1-t)(a,b) + t(c,d) = (a+t(c-a),b+t(d-b)) [0,1]

   public LineEquationImpl(Point2D point, Point2D point2) {
      m = (point.getY() - point2.getY()) / (point.getX() - point2.getX());
      c = point.getY() - m * point.getX();
      a = point.getX();
      b = point.getY();
      ct = point2.getX();
      d = point2.getY();
   }

   public LineEquationImpl(Point2D point, double lineLength, double angle) {
      m = (point.getY() - lineLength * Math.sin(angle)) / (point.getX() - lineLength * Math.cos(angle));
      c = point.getY() - m * point.getX();
      a = point.getX();
      b = point.getY();
      ct = lineLength * Math.cos(angle);
      d = lineLength * Math.sin(angle);
   }

   public double getCoeff() {
      return c;
   }

   public double getGradient() {
      return m;
   }

   public double getA() {
      return a;
   }

   public double getB() {
      return b;
   }

   public double getC() {
      return ct;
   }

   public double getD() {
      return d;
   }

}
