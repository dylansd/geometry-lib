package co.uk.ptl.indicators;

import java.text.DecimalFormat;

import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.vos.ProfitLevel;
import co.uk.ptl.vos.ProfitLevelImpl;

public class AbcdImpl implements Abcd {

   private final TurningPointVO a;
   private final TurningPointVO b;
   private final TurningPointVO c;
   private double d;

   public String toString() {
      DecimalFormat df = new DecimalFormat("#.##");
      return "   A=" + a + " 0.00%\n   B=" + b + " " + df.format(getBPercent()) + "%\n   C=" + c + " " + df.format(getCPercent()) + "%";
   }

   public AbcdImpl(TurningPointVO a, TurningPointVO b, TurningPointVO c) {
      this.a = a;
      this.b = b;
      this.c = c;
   }

   
   public TurningPointVO getA() {
      return a;
   }

   
   public TurningPointVO getB() {
      return b;
   }

   
   public TurningPointVO getC() {
      return c;
   }

   
   public ProfitLevel getProposedD(double estBPercentage, double estCPercentage) {
      if (b.getDataVO().getPrice() > a.getDataVO().getPrice()) {
         double XFACTOR = (b.getDataVO().getPrice() - a.getDataVO().getPrice()) * 100 / estBPercentage;
         double estC = a.getDataVO().getPrice() + (XFACTOR * estCPercentage / 100);
         double errorBar = Math.abs(c.getDataVO().getPrice() - estC);
         double percent = (errorBar / estC) * 100;
         if (percent > 0.01) { //Within 0.05 %
            return null;
         }
         long futureTs = c.getDataVO().getTimestamp() + (c.getDataVO().getTimestamp() - b.getDataVO().getTimestamp());
         d = a.getDataVO().getPrice() + XFACTOR;
         return new ProfitLevelImpl(a.getDataVO().getPrice() + XFACTOR, futureTs);
      }
      // Down trend
      double XFACTOR = (a.getDataVO().getPrice() - b.getDataVO().getPrice()) * 100 / estBPercentage;
      double estC = a.getDataVO().getPrice() - (XFACTOR * estCPercentage / 100);
      double errorBar = Math.abs(c.getDataVO().getPrice() - estC);
      double percent = (errorBar / estC) * 100;
      if (percent > 0.01) { //Within 0.05 %
         return null;
      }
      long futureTs = c.getDataVO().getTimestamp() + (c.getDataVO().getTimestamp() - b.getDataVO().getTimestamp());
      d = a.getDataVO().getPrice() - XFACTOR;
      return new ProfitLevelImpl(a.getDataVO().getPrice() - XFACTOR, futureTs);
   }

   
   public double getAPercent() {
      return 0.0;
   }

   
   public double getBPercent() {
      double total = d - a.getDataVO().getPrice();
      return (b.getDataVO().getPrice() - a.getDataVO().getPrice()) / total * 100;
   }

   
   public double getCPercent() {
      double total = d - a.getDataVO().getPrice();
      return (c.getDataVO().getPrice() - a.getDataVO().getPrice()) / total * 100;
   }

}
