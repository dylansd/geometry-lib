package com.dksd.cycling.strava.point;

import com.dksd.cycling.strava.common.Definitions;

/**
 * Point implementation usually. Basic xyz coord.
 * 
 * @author Dylan Scott-Dawkins
 */
public abstract class AbstractPoint implements Point {

	private static int globid;
	private final double x;
	private final double y;
	private final double z;
	private final int id = globid++;

	/**
	 * Ctor.
	 * 
	 * @param x
	 *            x
	 * @param y
	 *            y
	 * @param z
	 *            z
	 */
	public AbstractPoint(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Get the ID
	 * 
	 * @return id of point
	 */
	public int getID() {
		return id;
	}

	/**
	 * Get X
	 * 
	 * @return double x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Get Y
	 * 
	 * @return double y coord
	 */
	public double getY() {
		return y;
	}

	/**
	 * Get the Z direction
	 * 
	 * @return double Z coord
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Get the distance to a point
	 * 
	 * @param pt
	 *            point to cal dist to.
	 * @return double some measure of GPS distance. Scaling factor here?
	 */
	public double getDistanceTo(Point pt) {
		double lat1 = this.x;
		double lat2 = pt.getX();
		double lon1 = this.y;
		double lon2 = pt.getY();
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		double radius = Definitions.RADIUS_OF_EARTH;
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
				* Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = radius * c;
		return d;
	}


	public double getDistanceToQuick(Point pt) {
		double radius = Definitions.RADIUS_OF_EARTH;
		double lat1 = this.x;
		double lat2 = pt.getX();
		double lon1 = this.y;
		double lon2 = pt.getY();
		double d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
		                  Math.cos(lat1)*Math.cos(lat2) *
		                  Math.cos(lon2-lon1)) * radius;
		return d;
	}
	/**
	 * Get the height from this point to pt. Should be in meters
	 * 
	 * @param pt
	 *            point to
	 * @return double height to.
	 */
	public double getHeightTo(Point pt) {
		double p1 = this.getZ();
		double p2 = pt.getZ();
		if (p1 < p2) {
			return Math.abs(p2 - p1);
		}
		return 0;
	}

	public String toString() {
		return "id=" + id + " (x,y,z)=(" + x + "," + y + "," + z + ")";
	}

	public double getBearingTo(Point pt) {
		double dLon = pt.getY() - this.getY();
		double y = Math.sin(dLon) * Math.cos(pt.getX());
		double x = Math.cos(getX()) * Math.sin(pt.getX()) - Math.sin(getX())
				* Math.cos(pt.getX()) * Math.cos(dLon);
		double brng = Math.atan2(y, x);
		return brng;
	}

}
