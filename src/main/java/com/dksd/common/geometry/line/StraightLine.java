package co.uk.ptl.indicators;

import co.uk.ptl.turningpoint.TurningPointVO;

public interface StraightLine {

   TurningPointVO getStartTP();

   TurningPointVO getEndTP();

   double fox(double ts);

   long getIntersectTime(StraightLine line);

   double getC();

   double getGradient();

}
