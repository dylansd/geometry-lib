package co.uk.dksd.ptl.trading.algorithm;

public interface TurningPoint {

   boolean isHigh();

   PriceTime getTurningPointTime();

}
