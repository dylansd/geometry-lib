package co.uk.ptl.indicators;

import java.util.List;
import java.util.Vector;

import co.uk.ptl.common.PeriodProperties;
import co.uk.ptl.evaluation.MarketEvaluator;
import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.vos.ParseDataVO;
import co.uk.ptl.vos.ProfitLevel;

public interface AbcdOptimizer {

   List<ProfitLevel> getOptimalAbcd(Vector<TurningPointVO> turningPoints, MarketEvaluator statObj, Vector<ParseDataVO> marketHistory,
      ParseDataVO latestVo, PeriodProperties prop);

   void clear();

}
