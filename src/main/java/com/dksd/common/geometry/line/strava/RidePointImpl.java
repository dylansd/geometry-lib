package com.dksd.cycling.strava.point;

/**
 * Ride Point implementation. Contains details of GPS traces etc.
 * 
 * @author Dylan Scott-Dawkins
 */
public class RidePointImpl extends AbstractPoint implements RidePoint {

	private final double time;
	private final int hr;
	private final int cadence;

	/**
	 * Ctor.
	 * 
	 * @param x
	 *            x
	 * @param y
	 *            y
	 * @param z
	 *            z
	 * @param time
	 *            time
	 * @param hr
	 *            hr
	 * @param cadence
	 *            cadence
	 */
	public RidePointImpl(final double x, final double y, final double z,
			final double time, final int hr, final int cadence) {
		super(x, y, z);
		this.time = time;
		this.hr = hr;
		this.cadence = cadence;
	}

	/**
	 * Get time in long millis
	 * 
	 * @return long the time of point
	 */
	public double getTime() {
		return time;
	}

	/**
	 * Get cadence
	 * 
	 * @return int the cadence of rider
	 */
	public int getCadence() {
		return cadence;
	}

	/**
	 * Get Heart Rate
	 * 
	 * @return int get heart rate
	 */
	public int getHeartRate() {
		return hr;
	}

	public String toString() {
		return super.toString() + " Time=" + time + ",hr=" + hr + ",cadence="
				+ cadence;
	}

}
