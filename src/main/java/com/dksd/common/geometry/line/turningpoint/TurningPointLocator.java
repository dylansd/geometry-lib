package co.uk.ptl.turningpoint;

import java.util.List;

import co.uk.ptl.vos.ParseDataVO;

public interface TurningPointLocator {

   boolean update(ParseDataVO vo);

   List<TurningPointVO> filter(String symbol, boolean high);

   void clear();

}
