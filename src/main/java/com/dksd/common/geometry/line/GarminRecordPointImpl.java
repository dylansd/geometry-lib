package com.dksd.cycling.garmin.fit;

import com.dksd.cycling.strava.point.GarminRecordPoint;
import com.dksd.cycling.strava.point.RidePointImpl;

public class GarminRecordPointImpl extends RidePointImpl implements GarminRecordPoint {

   private final double temp;
   private final double vel;
   private final double distsofar;
   private final double riderDirection;
   private final int recordedPower;
   private int estimatedPower;

   public GarminRecordPointImpl(double temp, double vel, double distsofar, double x, double y, double z, double time, int hr, int cadence,
         double riderDirection) {
      super(x, y, z, time, hr, cadence);
      this.temp = temp;
      this.vel = vel;
      this.distsofar = distsofar;
      this.riderDirection = riderDirection;
      this.recordedPower = -1;

   }

   public GarminRecordPointImpl(double temp, double vel, double distsofar, double x, double y, double z, double time, int hr, int cadence,
         double riderDirection, int recordedPower) {
      super(x, y, z, time, hr, cadence);
      this.temp = temp;
      this.vel = vel;
      this.distsofar = distsofar;
      this.riderDirection = riderDirection;
      this.recordedPower = recordedPower;
   }

   @Override
   public double getTemperature() {
      return this.temp;
   }

   @Override
   public double getVelocity() {
      return this.vel;
   }

   @Override
   public double getDistanceSoFar() {
      return this.distsofar;
   }

   @Override
   public double getRiderDirection() {
      return riderDirection;
   }

   @Override
   public int getRecordedPower() {
      return this.recordedPower;
   }

   @Override
   public int getEstimatedPower() {
      return this.estimatedPower;
   }

}
