package com.dksd.cycling.strava.point;

public interface Elevation {

	/**
	 * @return
	 */
	double getSensorElevation();

	/**
	 * @return
	 */
	double getInterpolatedElevation();

	/**
	 * @return
	 */
	double getGoogleElevation();

}
