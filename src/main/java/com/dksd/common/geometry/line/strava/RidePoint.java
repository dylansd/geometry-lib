package com.dksd.cycling.strava.point;

/**
 * Describes the rider point. This includes where the rider is aka Point. Plus
 * stuff like time the rider was there, velocity, HR, Cadence.
 * 
 * @author dylan
 */
public interface RidePoint extends Point {

	/**
	 * Gets the time in seconds.
	 * 
	 * @return the time
	 */
	double getTime();

	/**
	 * Gets the cadence.
	 * 
	 * @return the cadence
	 */
	int getCadence();

	/**
	 * Gets the heart rate.
	 * 
	 * @return the heart rate
	 */
	int getHeartRate();

}
