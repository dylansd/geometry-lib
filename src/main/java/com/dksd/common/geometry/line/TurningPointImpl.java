package co.uk.dksd.ptl.trading.algorithm;

import co.uk.dksd.ptl.trading.Price;

public class TurningPointImpl implements TurningPoint {

   private Price tp;
   private boolean high;

   public TurningPointImpl(Price tp, boolean high) {
      this.tp = tp;
      this.high = high;
   }

   public PriceTime getTurningPointTime() {
      return tp.getTime();
   }

   public boolean isHigh() {
      return high;
   }

   public String toString() {
      return "" + tp;
   }

}
