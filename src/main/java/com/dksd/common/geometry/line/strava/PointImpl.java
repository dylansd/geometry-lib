package com.dksd.cycling.strava.point;

/**
 * Simple implementation that extends the Abstract Point implementation rather
 * 
 * @author Dylan Scott-Dawkins
 */
public class PointImpl extends AbstractPoint implements Point {

	/**
	 * Ctor
	 * 
	 * @param x
	 *            x
	 * @param y
	 *            y
	 * @param z
	 *            z
	 */
	public PointImpl(double x, double y, double z) {
		super(x, y, z);
	}

}

/*
 * import it.units.GoogleCommon.GeocodeException; import
 * it.units.GoogleCommon.Location; import
 * it.units.GoogleElevation.ElevationRequestor; import
 * it.units.GoogleElevation.ElevationResponse; import
 * it.units.GoogleElevation.Result; import it.units.examples.Main;
 * 
 * import java.util.ArrayList; import java.util.List; import
 * java.util.logging.Level; import java.util.logging.Logger;
 * 
 * 
 * public class Elevation {
 * 
 * public static void main(String[] args) { // TODO Auto-generated method stub
 * Location location = new Location(); location.setLat(51.248432f);
 * location.setLng(-0.312989f);
 * 
 * ElevationRequestor requestor = new ElevationRequestor(); ElevationResponse
 * elevationResponse; try { elevationResponse =
 * requestor.getElevation(location); System.out.println(elevationResponse); }
 * catch (GeocodeException ex) {
 * Logger.getLogger(Main.class.getName()).log(Level.SEVERE,
 * "Error fetching Elevation", ex); }
 * 
 * List<Location> locations = new ArrayList<Location>(); Location location1 =
 * new Location(); location1.setLat(39.7391536f);
 * location1.setLng(-104.9847034f); locations.add(location1); Location location2
 * = new Location(); location2.setLat(36.455556f);
 * location2.setLng(-116.866667f); locations.add(location2);
 * 
 * requestor = new ElevationRequestor(); try { elevationResponse =
 * requestor.getElevations(locations); for (Result result :
 * elevationResponse.getResults()){ System.out.println(result); }
 * System.out.println(elevationResponse); } catch (GeocodeException ex) {
 * Logger.getLogger(Main.class.getName()).log(Level.SEVERE,
 * "Error fetching Elevation", ex); }
 * 
 * }
 * 
 * }
 */
