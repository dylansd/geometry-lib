package co.uk.ptl.indicators;

import java.util.List;

import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.vos.ParseDataVO;

public interface SupportResistance {

   StraightLine generateSupport(List<ParseDataVO> history, List<TurningPointVO> tps) throws Exception;

   StraightLine generateResistance(List<ParseDataVO> history, List<TurningPointVO> tps) throws Exception;

   boolean verifyLastSupport(String symbol, double startPrice, double endPrice, long startTime, long endTime);

   boolean verifyLastResistance(String symbol, double startPrice, double endPrice, long startTime, long endTime);

   void clear();

}
