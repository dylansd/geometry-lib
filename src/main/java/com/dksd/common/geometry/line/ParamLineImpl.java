package co.uk.ptl.indicators;

public class ParamLineImpl implements ParamLine {

   private double x1;
   private double x2;
   private double y1;
   private double y2;
   private double m = 0;
   private double c = 0;

   public ParamLineImpl(double x1, double x2, double y1, double y2) {
      super();
      if (x1 > x2) {
         double tmp = x1;
         this.x1 = x2;
         this.x2 = tmp;
         tmp = y1;
         this.y1 = y2;
         this.y2 = tmp;
      } else {
         this.x1 = x1;
         this.x2 = x2;
         this.y1 = y1;
         this.y2 = y2;
      }
      try {
         m = 1 / ((double) x2 - (double) x1);
      } catch (Exception ep) {
         m = 9999999.0;
      }
      c = -m * (double) x1;
   }

   public double convertLongT(long timestamp) {
      return m * (double) timestamp + c;
   }

   public double getX(double t) {
      return x1 + t * (x2 - x1);
   }

   public double getY(double t) {
      return y1 + t * (y2 - y1);
   }

   public int compareTo(ParamLine o) {
      if (o.getM() == this.m && o.getC() == this.c) {
         return 0;
      }
      return -1;
   }

   public double getC() {
      return c;
   }

   public double getM() {
      return m;
   }

}
