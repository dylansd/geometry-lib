package co.uk.ptl.turningpoint;

import co.uk.ptl.vos.ParseDataVO;

public class CachedTurningPointInfo {

   private boolean isHighTP = false;
   private boolean isLowTP = false;
   private ParseDataVO dataVO = null;

   public boolean isHighTP() {
      return isHighTP;
   }

   public void setHighTP(boolean isHighTP) {
      this.isHighTP = isHighTP;
   }

   public boolean isLowTP() {
      return isLowTP;
   }

   public void setLowTP(boolean isLowTP) {
      this.isLowTP = isLowTP;
   }

   public ParseDataVO getDataVO() {
      return dataVO;
   }

   public void setDataVO(ParseDataVO dataVO) {
      this.dataVO = dataVO;
   }

}
