package co.uk.dksd.utils.geometry;

public class Line2DImpl implements Line2D {

   private final LineEquation lineEq;

   public Line2DImpl(LineEquation le) {
      this.lineEq = le;
   }

   public Point2D getParamCoord(double time) {
      //(x(t),y(t)) = (1-t)(a,b) + t(c,d) = (a+t(c-a),b+t(d-b)) [0,1]
      return new Point2DImpl(lineEq.getA() + time * (lineEq.getC() - lineEq.getA()), lineEq.getB() + time
            * (lineEq.getD() - lineEq.getB()), false);
   }

   public double getParamT(Point2D pt) {
      //x = a + t (c - a);
      //x-a/c-a = t;
      //y = b + t (d - b);
      //y-b/d-b
      //TODO is x is horizontal etc...
      return (pt.getX() - lineEq.getA()) / (lineEq.getC() - lineEq.getA());
   }

   public Point2D getXCoord(double y) {
      return new Point2DImpl((y - this.lineEq.getCoeff()) / this.lineEq.getGradient(), y, false);
   }

   public Point2D getYCoord(double x) {
      return new Point2DImpl(x, this.lineEq.getGradient() * x + this.lineEq.getCoeff(), false);
   }

   public Point2D calcIntersect(Line2D line) {
      //m1x+c1 = m2x+c2;
      //m1x = m2x+c2 - c1;
      //(m1-m2)x = c2-c1;
      //x= (c2 - c1) / (m1 - m2);
      //y=mx+c;
      double x = (lineEq.getCoeff() - line.getEquation().getCoeff()) / (line.getEquation().getCoeff() - lineEq.getCoeff());
      double y = lineEq.getGradient() * x + lineEq.getCoeff();
      return new Point2DImpl(x, y, false);
   }

   public LineEquation getEquation() {
      return lineEq;
   }

}
