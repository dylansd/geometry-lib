package co.uk.ptl.turningpoint;

import java.util.Date;

import co.uk.ptl.vos.ParseDataVO;

public class TurningPointVOImpl implements TurningPointVO {

   private final ParseDataVO dataVo;
   private final boolean highTP;
   private boolean ignore = false;

   public TurningPointVOImpl(final ParseDataVO dataVo, final boolean highTp) {
      this.dataVo = dataVo;
      this.highTP = highTp;
   }

   public ParseDataVO getDataVO() {
      return dataVo;
   }

   public boolean isHighTurningpoint() {
      return highTP;
   }

   public String toString() {
      return "" + this.dataVo.getPrice() + ", " + new Date(dataVo.getTimestamp()) + " id: " + this.dataVo.getPrice(); // /*(dataVo.getTimestamp()/(1000*60))*/ + " HighTP:" + this.highTP;
   }

   public boolean checkSymbol(String symbol) {
      return this.dataVo.getSymbol().equals(symbol);
   }


   public int compareTo(TurningPointVO o) {
      if (this.getDataVO().getPrice() > o.getDataVO().getPrice()) {
         return 1;
      }
      if (this.getDataVO().getPrice() < o.getDataVO().getPrice()) {
         return -1;
      }
      return 0;
   }


   public void setIgnore() {
      ignore = true;
   }


   public boolean isIgnoreTP() {
      return ignore;
   }

}
