package com.dksd.common.geometry.line;

public class Point2DImpl implements Point2D {

   private final double x;
   private final double y;
   private final boolean isFixed;

   public Point2DImpl(double x, double y, boolean isFixed) {
      this.x = x;
      this.y = y;
      this.isFixed = isFixed;
   }

   public double getX() {
      return x;
   }

   public double getY() {
      return y;
   }

   public boolean isFixed() {
      return isFixed;
   }

}
