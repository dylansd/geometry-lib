package cycling.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Document(collection="GpsPoint")
public class GpsPoint {

    @Id
    private long ts;
    private float lat;
    private float lon;
    private double vel;
    private double bearing;
    private float power;
    private int hr;
    private float elevation;
}
