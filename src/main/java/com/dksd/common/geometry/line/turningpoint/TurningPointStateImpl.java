package co.uk.ptl.turningpoint;

import java.util.HashSet;
import java.util.Set;

public class TurningPointStateImpl implements TurningPointState {

   private int lastIndex = -1;
   private Set<TurningPointVO> tps = new HashSet<TurningPointVO>();

   public int getIndex() {
      return lastIndex;
   }

   public Set<TurningPointVO> getPrevTps() {
      return tps;
   }

   public void updateState(int startIndex, Set<TurningPointVO> tps) {
      this.lastIndex = startIndex;
      this.tps = tps;
   }

}
