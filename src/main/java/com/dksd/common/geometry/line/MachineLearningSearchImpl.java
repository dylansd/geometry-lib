package co.uk.ptl.indicators;

import java.util.Vector;

import co.uk.ptl.common.DateUtils;
import co.uk.ptl.vos.ParseDataVO;

public class MachineLearningSearchImpl implements MachineLearningSearch {

   public void search(Vector<ParseDataVO> marketHistory) {
      if (!DateUtils.isOptimalTradeWindow(marketHistory.get(marketHistory.size() - 1))) {
         return;
      }
      ParseDataVO start = null;
      ParseDataVO mid = null;
      ParseDataVO end = null;
      double maxMid = 0;
      double maxEnd = 0;
      for (ParseDataVO pso : marketHistory) {
         start = pso;
         for (ParseDataVO pso2 : marketHistory) {
            double midval = pso2.getPrice() - start.getPrice();
            if (midval > maxMid) {
               mid = pso2;
               maxMid = midval;
               end = pso2;
               maxEnd = 0;
            }
            if (mid != null) {
               double endval = mid.getPrice() - pso2.getPrice();
               if (endval > maxEnd) {
                  end = pso2;
                  maxEnd = endval;
               }
            }
         }
      }
      if (maxMid > 0.004 && maxEnd > 0.004) {
         //System.out.println("Should be a warnign to ya.");		
      }
   }

}
