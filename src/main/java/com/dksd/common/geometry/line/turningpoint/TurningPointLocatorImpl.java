package co.uk.ptl.turningpoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.uk.ptl.common.PeriodProperties;
import co.uk.ptl.vos.ParseDataVO;

public class TurningPointLocatorImpl implements TurningPointLocator {

   private Map<String, List<TurningPointVO>> tps = new HashMap<String, List<TurningPointVO>>();
   private Map<String, List<ParseDataVO>> buffer = new HashMap<String, List<ParseDataVO>>();
   private final PeriodProperties prop;

   public TurningPointLocatorImpl(PeriodProperties prop) {
      this.prop = prop;
   }

   private boolean isTp(List<ParseDataVO> buf, int index, boolean high) {
      double indValue = buf.get(index).getPrice();
      for (int i = 0; i < buf.size(); i++) {
         if (index != i) {
            if (high) {
               if (buf.get(i).getPrice() > indValue) {
                  return false;
               }
            } else {
               if (buf.get(i).getPrice() < indValue) {
                  return false;
               }
            }
         }
      }
      return true;
   }

   public List<TurningPointVO> filter(String symbol, boolean filterHighTPs) {
      List<TurningPointVO> stps = this.tps.get(symbol);
      if (stps == null) {
         return null;
      }
      List<TurningPointVO> ret = new ArrayList<TurningPointVO>();
      for (TurningPointVO tp : stps) {
         if (tp.isHighTurningpoint() == filterHighTPs) {
            ret.add(tp);
         }
      }
      return ret;
   }

   public boolean update(ParseDataVO pso) {
      List<ParseDataVO> dbuf = this.buffer.get(pso.getSymbol());
      if (dbuf == null) {
         dbuf = new ArrayList<ParseDataVO>();
         this.buffer.put(pso.getSymbol(), dbuf);
      }
      dbuf.add(pso);
      List<TurningPointVO> stps = this.tps.get(pso.getSymbol());
      if (stps == null) {
         stps = new ArrayList<TurningPointVO>();
         this.tps.put(pso.getSymbol(), stps);
      }
      return processBuffer(dbuf, stps);
   }

   private boolean processBuffer(List<ParseDataVO> dbuf, List<TurningPointVO> stps) {
      int window = (int) prop.getAbcdWindow();
      if (dbuf.size() < window) {
         return false;
      }
      int index = dbuf.size() - (window / 2);
      if (isTp(dbuf, index, true)) {
         stps.add(new TurningPointVOImpl(dbuf.get(index), true));
      } else if (isTp(dbuf, index, false)) {
         stps.add(new TurningPointVOImpl(dbuf.get(index), false));
      }
      if (dbuf.size() >= window) {
         dbuf.remove(0);
      }
      return true;
   }


   public void clear() {
      tps.clear();
      buffer.clear();
   }

}
