package com.dksd.common.geometry.line;

public interface LineEquation {

   double getGradient();

   double getCoeff();

   double getA();

   double getB();

   double getC();

   double getD();

}
