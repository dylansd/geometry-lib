package com.dksd.cycling.common.point;

public interface GarminRecordPoint extends RidePoint {

   /**
    * Get the temperature at that time for the point
    * 
    * @return
    */
   double getTemperature();

   /**
    * .fit files calc the velocity already so use that
    * 
    * @return
    */
   double getVelocity();

   /**
    * Distance so far
    * 
    * @return
    */
   double getDistanceSoFar();

   double getRiderDirection();

   int getRecordedPower();

   int getEstimatedPower();

}
