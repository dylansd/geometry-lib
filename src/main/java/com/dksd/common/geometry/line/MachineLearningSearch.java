package co.uk.ptl.indicators;

import java.util.Vector;

import co.uk.ptl.vos.ParseDataVO;

public interface MachineLearningSearch {

   void search(Vector<ParseDataVO> marketHistory);

}
