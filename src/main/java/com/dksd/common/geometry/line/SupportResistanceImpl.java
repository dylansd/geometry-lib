package co.uk.ptl.indicators;

import java.util.ArrayList;
import java.util.List;

import co.uk.ptl.common.Definitions;
import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.turningpoint.TurningPointVOImpl;
import co.uk.ptl.vos.ParseDataVO;
import co.uk.ptl.vos.ParseDataVOImpl;

public class SupportResistanceImpl implements SupportResistance {

   private List<StraightLine> candidateLines = new ArrayList<StraightLine>();
   private TurningPointVO maxTp = null;
   private TurningPointVO minTp = null;
   private StraightLine lastSupport;
   private StraightLine lastResistance;

   public SupportResistanceImpl() {
   }

   
   public StraightLine generateSupport(List<ParseDataVO> history, List<TurningPointVO> tps) throws Exception {
      findMinMaxPoints(history.get(history.size() - 1), tps, false);
      TurningPointVO sisterTP = createSisterTP(this.minTp, false);
      StraightLine ret = new StraightLineImpl(this.minTp, sisterTP);
      this.lastSupport = ret;
      return ret;
      /*
       * candidateLines.clear(); maxTp = null; minTp = null;
       * wrapTurningPoints(history, tps, false); while (candidateLines.size()
       * == 0) { wrapTurningPoints(history, tps, false); } StraightLine ans =
       * bestCandidate(); if (ans == null) { dumpTurningPoints(tps);
       * dumpHistory(history); throw new
       * Exception("Support line cannot be null!!!!"); } this.lastSupport =
       * ans; return ans;
       */
   }

   
   public StraightLine generateResistance(List<ParseDataVO> history, List<TurningPointVO> tps) throws Exception {
      findMinMaxPoints(history.get(history.size() - 1), tps, true);
      TurningPointVO sisterTP = createSisterTP(this.maxTp, true);
      StraightLine ret = new StraightLineImpl(this.maxTp, sisterTP);
      this.lastResistance = ret;
      return ret;
      /*
       * candidateLines.clear(); maxTp = null; minTp = null;
       * wrapTurningPoints(history, tps, true); while (candidateLines.size()
       * == 0) { wrapTurningPoints(history, tps, false); } StraightLine ans =
       * bestCandidate(); if (ans == null) { dumpTurningPoints(tps);
       * dumpHistory(history); throw new
       * Exception("Resistance line cannot be null!!!!"); }
       * this.lastResistance = ans; return ans;
       */
   }

   private TurningPointVO createSisterTP(TurningPointVO first, boolean high) {
      ParseDataVO po = first.getDataVO();
      ParseDataVO sisFakeVo = new ParseDataVOImpl(1, po.getSymbol(), "" + po.getPrice(), po.getTimestamp() + 1000, "" + po.getLow(), ""
            + po.getHigh());
      return new TurningPointVOImpl(sisFakeVo, high);
   }

   private void dumpHistory(List<ParseDataVO> history) {
      for (ParseDataVO pso : history) {
         Definitions.debug("SuportRes", pso.toString());
      }
   }

   private void dumpTurningPoints(List<TurningPointVO> tps) {
      for (TurningPointVO tp : tps) {
         Definitions.debug("TurningPoint", tp.toString());
      }
   }

   private StraightLine bestCandidate() {
      long bigDiff = 0;
      StraightLine retLine = null;
      for (StraightLine line : candidateLines) {
         long diff = line.getEndTP().getDataVO().getTimestamp() - line.getStartTP().getDataVO().getTimestamp();
         if (diff > bigDiff) {
            bigDiff = diff;
            retLine = line;
         }
      }
      return retLine;
   }

   void findMinMaxPoints(ParseDataVO pso, List<TurningPointVO> tps, boolean high) {
      for (TurningPointVO tpouter : tps) {
         if (pso.getTimestamp() - tpouter.getDataVO().getTimestamp() < Definitions.MIN_SUPPORT_TIME_DISTANCE) {
            if (high) {
               if (maxTp == null) {
                  maxTp = tpouter;
               }
               if (tpouter.isHighTurningpoint() && tpouter.getDataVO().getPrice() > this.maxTp.getDataVO().getPrice()) {
                  maxTp = tpouter;
               }
            } else {

               if (minTp == null) {
                  minTp = tpouter;
               }

               if (!tpouter.isHighTurningpoint() && tpouter.getDataVO().getPrice() < this.minTp.getDataVO().getPrice()) {
                  minTp = tpouter;
               }
            }
         }
      }
   }

   private void wrapTurningPoints(List<ParseDataVO> history, List<TurningPointVO> tps, boolean high) {
      if (tps == null) {
         return;
      }
      //findMinMaxPoints(tps, high);
      for (TurningPointVO tpouter : tps) {
         if (!tpouter.isIgnoreTP()) {
            long tpouterTS = tpouter.getDataVO().getTimestamp();
            for (TurningPointVO tpinner : tps) {
               if (!tpinner.isIgnoreTP()) {
                  long tpinnerTS = tpinner.getDataVO().getTimestamp();
                  if (tpinnerTS != tpouterTS && tpinnerTS - tpouterTS > Definitions.MIN_SUPPORT_TIME_DISTANCE) {
                     StraightLine line = new StraightLineImpl(tpouter, tpinner);
                     if (verifyLine(tps, history, line, high)) {
                        candidateLines.add(line);
                     }
                  }
               }
            }
         }
      }
      int i = 0;
      while (i < tps.size()) {
         TurningPointVO vo = tps.get(i);
         if (vo.isIgnoreTP()) {
            tps.remove(i);
         } else {
            i++;
         }
      }
   }

   private void determineIgnore(List<TurningPointVO> tps, StraightLine line, boolean high) {
      if (tps == null) {
         return;
      }
      if (tps.size() == 0) {
         return;
      }
      for (int i = 0; i < tps.size(); i++) {
         TurningPointVO vo = tps.get(i);
         double p = vo.getDataVO().getPrice();
         double cprice = line.fox(vo.getDataVO().getTimestamp());
         if (high && vo.isHighTurningpoint()) {
            if (p < cprice && Math.abs(cprice - p) > 0.00001) {
               vo.setIgnore();
            }
         } else if (!high && !vo.isHighTurningpoint()) {
            if (p > cprice && Math.abs(cprice - p) > 0.00001) {
               vo.setIgnore();
            }
         }
      }
   }

   private boolean verifyLine(List<TurningPointVO> tps, List<ParseDataVO> history, StraightLine candidate, boolean high) {
      if (candidate.getEndTP() == null | candidate.getStartTP() == null) {
         return false;
      }
      if (!verifyMaxMin(high, candidate)) {
         return false;
      }
      determineIgnore(tps, candidate, high);
      for (int i = 0; i < history.size(); i++) {
         ParseDataVO vo = history.get(i);
         double p = vo.getPrice();
         long vots = vo.getTimestamp();
         if (vots > candidate.getStartTP().getDataVO().getTimestamp()/*
                                                                     * &&
                                                                     * vots
                                                                     * <
                                                                     * candidate
                                                                     * .
                                                                     * getEndTP
                                                                     * ().
                                                                     * getDataVO
                                                                     * ().
                                                                     * getTimestamp
                                                                     * ()
                                                                     */) {
            double cprice = candidate.fox(vo.getTimestamp());
            if (high) {
               if (p > cprice && Math.abs(cprice - p) > 0.0001) {
                  return false;
               }
            } else {
               if (p < cprice && Math.abs(cprice - p) > 0.0001) {
                  return false;
               }
            }
         }
      }
      return true;
   }

   private boolean verifyMaxMin(boolean high, StraightLine candidate) {
      if (high) {
         double maxprice = candidate.fox(this.maxTp.getDataVO().getTimestamp());
         if (maxprice < this.maxTp.getDataVO().getPrice()) {
            return false;
         }
      } else {
         double minprice = candidate.fox(this.minTp.getDataVO().getTimestamp());
         if (minprice > this.minTp.getDataVO().getPrice()) {
            return false;
         }
      }
      return true;
   }

   
   public boolean verifyLastSupport(String symbol, double startPrice, double endPrice, long startTime, long endTime) {
      boolean valid = true;
      if (lastSupport == null) {
         return false;
      }
      if (startPrice != lastSupport.getStartTP().getDataVO().getPrice()) {
         valid = false;
      }
      if (endPrice != lastSupport.getEndTP().getDataVO().getPrice()) {
         valid = false;
      }
      if (startTime != lastSupport.getStartTP().getDataVO().getTimestamp()) {
         valid = false;
      }
      if (endTime != lastSupport.getEndTP().getDataVO().getTimestamp()) {
         valid = false;
      }
      return valid;
   }

   
   public boolean verifyLastResistance(String symbol, double startPrice, double endPrice, long startTime, long endTime) {
      boolean valid = true;
      if (lastResistance == null) {
         return false;
      }
      double value = lastResistance.getStartTP().getDataVO().getPrice();
      if (startPrice != value) {
         valid = false;
      }
      value = lastResistance.getEndTP().getDataVO().getPrice();
      if (endPrice != value) {
         valid = false;
      }
      long valuel = lastResistance.getStartTP().getDataVO().getTimestamp();
      if (startTime != valuel) {
         valid = false;
      }
      valuel = lastResistance.getEndTP().getDataVO().getTimestamp();
      if (endTime != value) {
         valid = false;
      }
      return valid;
   }

   
   public void clear() {
      candidateLines.clear();
      maxTp = null;
      minTp = null;
      lastSupport = null;
      lastResistance = null;
   }
}
