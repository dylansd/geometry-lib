package co.uk.ptl.indicators;

public interface ParamLine extends Comparable<ParamLine> {

   double getX(double t);

   double getY(double t);

   double convertLongT(long timestamp);

   double getM();

   double getC();

}
