package co.uk.ptl.turningpoint;

import java.util.Set;

public interface TurningPointState {

   int getIndex();

   Set<TurningPointVO> getPrevTps();

   void updateState(int startIndex, Set<TurningPointVO> tps);

}
