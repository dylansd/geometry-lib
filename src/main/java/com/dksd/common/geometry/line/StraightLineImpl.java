package co.uk.ptl.indicators;

import co.uk.ptl.turningpoint.TurningPointVO;
import co.uk.ptl.turningpoint.TurningPointVOImpl;

public class StraightLineImpl implements StraightLine {

   private boolean infiniteGradient = false;
   private double m = 0;
   private double c = 0;
   private final TurningPointVO start;
   private final TurningPointVO end;

   public StraightLineImpl(TurningPointVO tp1, TurningPointVO tp2) {
      this.start = tp1;
      this.end = tp2;
      constructLine(tp1, tp2);
   }

   public StraightLineImpl(TurningPointVO maxTp) {
      this.start = maxTp;
      this.end = maxTp;
      constructLine(start, end);
   }

   private void constructLine(TurningPointVO tp1, TurningPointVO tp2) {
      // y = mx + c
      double x1 = tp1.getDataVO().getTimestamp();
      double x2 = tp2.getDataVO().getTimestamp();
      if (x1 == x2) {
         x2 = x1 + 1;
      }
      double y1 = tp1.getDataVO().getPrice();
      double y2 = tp2.getDataVO().getPrice();
      if (x2 - x1 == 0) {
         infiniteGradient = true;
      }
      m = (y2 - y1) / (x2 - x1);
      c = y1 - m * x1;
   }

   
   public double fox(double ts) {
      return m * (double) ts + c;
   }

   
   public TurningPointVO getEndTP() {
      return end;
   }

   
   public TurningPointVO getStartTP() {
      return start;
   }

   
   public long getIntersectTime(StraightLine line) {
      return (long) ((line.getC() - getC()) / (getGradient() - line.getGradient()));
   }

   
   public double getC() {
      return c;
   }

   
   public double getGradient() {
      return m;
   }

   
   public String toString() {
      return "" + m + "x + " + c + " start: " + getStartTP() + " endTP: " + getEndTP();
   }

}
