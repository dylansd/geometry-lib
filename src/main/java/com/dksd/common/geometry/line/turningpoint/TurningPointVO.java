package co.uk.ptl.turningpoint;

import co.uk.ptl.vos.ParseDataVO;

public interface TurningPointVO extends Comparable<TurningPointVO> {

   ParseDataVO getDataVO();

   boolean isHighTurningpoint();

   boolean checkSymbol(String symbol);

   boolean isIgnoreTP();

   void setIgnore();

}
