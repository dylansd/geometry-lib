package com.dksd.cycling.garmin.fit;

import com.dksd.cycling.strava.point.AbstractPoint;

public class DistancePointImpl extends AbstractPoint {

   public DistancePointImpl(double x, double y, double z) {
      super(x, y, z);
   }

}
