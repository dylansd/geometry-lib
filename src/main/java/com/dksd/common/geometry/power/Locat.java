package com.dksd.gpxloader.track;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Locat {

    private int curveId;
    private double lat;
    private double lon;
    private double power;
    private int hb;
    private long time;
    private double alt;
    private String name = null;
    private double smoothAlt;

    public Locat() {
    }

    public Locat(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double calcSpeed(Locat prevPoint) {
        return distanceTo(prevPoint) / ((this.time - prevPoint.getTime()) / 1000);
    }

    private double driveEfficiency = 0.96;

    public double calcPower(double vms, double cda, double totalMass, double slope) {
        return (getPowerDrag(vms, cda)
                + getPowerRolling(vms, totalMass)
                + getPowerClimbing(vms, totalMass, slope)) / driveEfficiency;
    }

    private double getPowerRolling(double vms, double riderMass) {
        return vms * riderMass * 9.8 * 0.005 /*Crr*/;
    }

    private double getPowerClimbing(double vms, double riderMass, double slope) {
        return vms * riderMass * 9.8 * slope;
    }

    private double getPowerDrag(double vms, double cda) {
        return 0.5 * vms * vms * vms * cda;
    }

    public double calcSlope(Locat prevPoint) {
        double dist = distanceTo(prevPoint);
        double rise = getSmoothAlt() - prevPoint.getSmoothAlt();
        if (dist < 0.001) {
            return 0;
        }
        return rise / dist;
    }

    public double distanceTo(Locat p2) {
        double R = 6372.8; // km
        double dLat = Math.toRadians(p2.getLatitude() - getLatitude());
        double dLon = Math.toRadians(p2.getLongitude() - getLongitude());
        double lat1 = Math.toRadians(getLatitude());
        double lat2 = Math.toRadians(p2.getLatitude());

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c * 1000.0;
    }

    public double bearingTo(Locat point2) {
        double lon1 = Math.toRadians(getLongitude());
        double lon2 = Math.toRadians(point2.getLongitude());
        double lat1 = Math.toRadians(getLatitude());
        double lat2 = Math.toRadians(point2.getLatitude());
        double y = Math.sin(lon2 - lon1) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) -
                Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);
        return Math.atan2(y, x);
    }

    public Locat addDistanceToGps(double dist, double brng) {
        double R = 6371; // km
        double d = dist / 1000.0;
        double lat1 = Math.toRadians(getLatitude());
        double lon1 = Math.toRadians(getLongitude());
        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(d / R) +
                Math.cos(lat1) * Math.sin(d / R) * Math.cos(brng));
        double lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(d / R) * Math.cos(lat1),
                Math.cos(d / R) - Math.sin(lat1) * Math.sin(lat2));
        return new Locat(Math.toDegrees(lat2), Math.toDegrees(lon2));
    }

    public double getLatitude() {
        return this.lat;
    }

    public double getLongitude() {
        return this.lon;
    }

    public double getAltitude() {
        return this.alt;
    }

    public String toString() {
        return "" + lat + "," + lon + "," + smoothAlt;
    }

    public String toGpx() {

        long val = time;
        Date date = new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String dateText = df2.format(date);

        String ans = "<trkpt lat=\"" + lat + "\" lon=\"" + lon + "\">" +
                "<ele>" + alt + "</ele>" +
                "<time>" + dateText + "</time> <extensions>" +
                "<gpxtpx:TrackPointExtension>" +
                "<gpxtpx:hr>" + hb + "</gpxtpx:hr>" +
                "</gpxtpx:TrackPointExtension>" + "</extensions>" + "</trkpt>";
        return ans;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getPower() {
        return power;
    }

    public void setSmoothAlt(double smoothAlt) {
        this.smoothAlt = smoothAlt;
    }

    public double getSmoothAlt() {
        return smoothAlt;
    }

    public void setLatitude(double parseDouble) {
        this.lat = parseDouble;
    }

    public void setLongitude(double parseDouble) {
        this.lon = parseDouble;
    }

    public void setAltitude(double alt) {
        this.alt = alt;
    }

    public void setHB(int hb) {
        this.hb = hb;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public long getTime() {
        return time;
    }

    public int getCurveId() {
        return curveId;
    }

    public void setCurveId(int curveId) {
        this.curveId = curveId;
    }
}
