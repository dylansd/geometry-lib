package com.dksd.virtcycler;

import com.dksd.common.app.networking.WebsocketMessage;
import com.dksd.common.app.networking.WebsocketMessageFactory;
import com.dksd.common.app.networking.WebsocketMessageFactoryImpl;
import com.dksd.comms.websocket.AbstractServer;
import com.dksd.gpxloader.track.GpxReader;
import com.dksd.gpxloader.track.Locat;
import com.google.gson.Gson;
//import org.alternativevision.gpx.GPXParser;
//import org.alternativevision.gpx.beans.GPX;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Map;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dylan on 06/03/15.
 */
public class VirtualCycler extends AbstractServer implements IVirtualCycler {
    private static final double MAX_ACCEL = 1;
    private static final double MAX_SLOPE = 0.1;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Vector<TimeNode> avg = new Vector<>();
    private final String name;
    private Vector<Locat> gpx = null;
    private long simTime = -1;
    private double m = 90;
    private double mg = 9.8 * m;
    private double dragCoeff = 0.2;
    private Locat current = null;
    private Locat next = null;
    private double distTravelledPerWaypoint = 0;
    private double vlocity = 0;
    private Gson gson = new Gson();
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Map<Locat, BufferedImage> locatToImages = new ConcurrentHashMap<>();
    private JFrame frame;
    private JLabel imgLbl;
    private JLabel velLbl;
    private JLabel distLbl;
    private JLabel powerLbl;
    private JLabel placeLbl;
    private JLabel slopeLbl;
    private JLabel hbLbl;
    private Vector<Locat> userActivity = new Vector<>();
    private double totalDistance = 0;
    private double totalJoules = 0;
    private long lastUserActivitySave = 0;
    private final File userSaveFile;
    private final File completedCourses = new File("/home/dylan/Dropbox/Dylan/Cycling/RidesVirtual/completedCourses");
    private boolean disableImagery = false;
    private JLabel jouleLbl;
    private double slope = 0;
    private Map<Locat, Boolean> requestedDownload = new ConcurrentHashMap<>();

    public VirtualCycler(final int port, final String name, final Vector<Locat> gpx, final File userSaveFile) throws InterruptedException, IOException {
        super(port);
        this.name = name;
        this.userSaveFile = userSaveFile;
        frame = new JFrame();
        FlowLayout layout = new FlowLayout();
        frame.setLayout(layout);
        frame.setSize(640, 640);
        imgLbl = new JLabel();
        frame.add(imgLbl);
        velLbl = new JLabel();
        velLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        distLbl = new JLabel();
        distLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        powerLbl = new JLabel();
        powerLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        placeLbl = new JLabel();
        placeLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        jouleLbl = new JLabel();
        jouleLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        hbLbl = new JLabel();
        hbLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        slopeLbl = new JLabel();
        slopeLbl.setFont(new Font("Serif", Font.PLAIN, 24));
        frame.add(velLbl);
        frame.add(distLbl);
        frame.add(slopeLbl);
        frame.add(powerLbl);
        frame.add(hbLbl);
        frame.add(jouleLbl);
        frame.add(placeLbl);
        frame.setVisible(true);
        loadRoute(name, gpx);
    }

    public void process(final WebsocketMessage wm) {
        final java.util.List<String> hbs = new ArrayList<>(wm.getPayload().get("hb"));
        final java.util.List<String> tss = new ArrayList<>(wm.getPayload().get("ts"));
        for (int i = 0 ; i < hbs.size(); i++) {
            String ts = tss.get(i);
            String hb = hbs.get(i);
            long timest = Long.parseLong(ts);
            updateAvhHr(timest, Integer.parseInt(hb));
            Locat curr = update(timest);
            if (curr == null) {
                saveUserActivity(userSaveFile);
                saveLastKnownPosition(current, name);
                System.exit(0);
            }
        }
    }

    @Override
    public double updateAvhHr(final long now, final int hb) {
        avg.add(new TimeNode(now, hb));
        return getAvhHr(now);
    }

    private double getAvhHr(final long now) {
        double total = 0;
        final Vector<TimeNode> rem = new Vector<>();
        for (TimeNode tn : avg) {
            if (tn.getTime() >= now - 3000) {
                total += tn.getHeartBeat();
            } else {
                rem.add(tn);
            }
        }
        for (final TimeNode tt : rem) {
            avg.remove(tt);
        }
        if (avg.size() == 0) {
            return 0;
        }
        return total / avg.size();
    }

    private Slope getSlope(Locat curr, Locat nxt) {
        try {
            double dist = nxt.distanceTo(curr);
            if (dist < 0.1) {
                return new Slope(0, 0);
            }
            double grad = (nxt.getAltitude() - curr.getAltitude()) / dist;
            double actual = grad;
            if (grad > MAX_SLOPE) {
                grad = MAX_SLOPE;
            }
            if (grad < -MAX_SLOPE) {
                grad = -MAX_SLOPE;
            }
            return new Slope(actual, grad);
        } catch(Exception ep) {
            ep.printStackTrace();
        }
        return null;
    }

    public int getPower(final double hr) {
        if (hr < 80) { //tricky, should measure change in hr, beed to extract 3 second average code to track accel
            return 0;
        }
        return (int) (0.012596 * hr * hr - 1.404774 * hr + 117.7847);
        //return (int) (0.017591849 * hr * hr - 2.17103249 * hr + 133);
    }

    @Override
    public Locat update(final long now) {
        double hr = getAvhHr(now);
        if (simTime == -1) {
            simTime = now;
        }
        if (now < simTime) {
            System.out.println("Error, hb time cannot be less than simulation time");
            return null;
        }
        double timeDiff = now - simTime;
        simTime = now;
        Slope slopeObject = getSlope(current, next);
        if (slopeObject.getVirtual() > slope) {
            slope += 0.0025;
        } else {
            slope -= 0.0025;
        }
        double power = getPower(hr);
        double newVel = getVelocity(slope, power);
        double tdsec = timeDiff/1000.0;
        totalJoules += power * tdsec;
        double a = (newVel - vlocity) / tdsec;
        if (a > MAX_ACCEL) {
            a = MAX_ACCEL;
        }
        if (a < -MAX_ACCEL) {
            a = -MAX_ACCEL;
        }
        vlocity = vlocity + a * tdsec;
        if (vlocity < 0) {
            vlocity = 0;
        }

        double dist = vlocity * tdsec;
        distTravelledPerWaypoint += dist;
        totalDistance += dist;
        writeActivityPoint(now, current, hr, power, power * timeDiff);

        distLbl.setText(" Dist: " + (int)totalDistance);
        jouleLbl.setText(" Joules: " + (int)totalJoules);
        velLbl.setText(" Vel: " + (int)(vlocity * 3.6));
        hbLbl.setText(" HR: " + (int)hr);
        slopeLbl.setText(" Slope: " + (int)(slopeObject.getActual()*100) + ":" + (int)(slope*100));
        powerLbl.setText(" Power: " + (int)power);

        System.out.println("Power: " + power);
        System.out.println("HR: " + hr);
        System.out.println("Dist travelled so far: " + totalDistance);
        System.out.println("Vel: " + (vlocity*3.6));
        System.out.println("Acceleration: " + a);
        System.out.println("Actual Slope: " + slopeObject.getActual());
        System.out.println("Virtual Slope: " + slope);

        double distanceBetweenPoints = current.distanceTo(next);
        if (distTravelledPerWaypoint > distanceBetweenPoints) {
            distTravelledPerWaypoint = distTravelledPerWaypoint - distanceBetweenPoints;
            current = next;
            displayImage(current);
            next = getNext(next);
            if (next == null || current == null) {
                saveUserActivity(userSaveFile);
                if (gpx.size() - gpx.indexOf(current) < 5) {
                    appendFile(completedCourses, name);
                }
                return null;
            }
            if (now > lastUserActivitySave + 1000 * 30) {
                saveUserActivity(userSaveFile);
                saveLastKnownPosition(current, name);
                //GeoName place = reverseGeoCode.nearestPlace(current.getLatitude(), current.getLongitude());
                //Locat plc = new Locat(place.latitude, place.longitude);
                //String placeStr = " Location : " + place.toString() + " ("+plc.distanceTo(current)+"km away)";
                //placeLbl.setText(placeStr);
                //System.out.println(placeStr);
                lastUserActivitySave = now;
            }
            int far = gpx.indexOf(current)+20;
            if (far < gpx.size()) {
                precacheDownloadImage(gpx.get(far-1), gpx.get(far));
            }
        }
        return current;
    }

    private void saveLastKnownPosition(final Locat current, final String routeName) {
        String lst = name + "," + current.toString() + "," + gpx.indexOf(current);
        try {
            FileUtils.write(new File(getLastPosition()), lst);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getLastPosition() {
        return "/home/dylan/Dropbox/Dylan/Cycling/RidesVirtual/lastKnownPosition" + name;
    }

    public void appendFile(final File file, final String data) {
        try {
            FileWriter writer = new FileWriter(file, true);
            writer.append(data);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveUserActivity(final File userSaveFile) {
        //Construct gpx file
        java.util.List<String> lnes = new ArrayList<>();
        for (Locat lc : userActivity) {
            lnes.add(lc.toGpx());
        }
        try {
            FileUtils.writeLines(userSaveFile, lnes);
            //gpxParser.writeGPX();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeActivityPoint(final long now,
                                    final Locat current,
                                    final double hr, final double power,
                                    final double joule) {
        Locat newPt = new Locat(current.getLatitude(), current.getLongitude());
        newPt.setAltitude(current.getAltitude());
        newPt.setPower(power);
        newPt.setHB((int) hr);
        newPt.setTime((int) now);
        newPt.setJoule(joule);
        userActivity.add(newPt);
        //Figure this stuff out
        //Track trkPt = new Track();
        //trkPt.addExtensionData();
        //userGpxActivity.addTrack(trkPt);
    }

    private Locat getNext(Locat next) {
        try {
            return gpx.get(gpx.indexOf(next) + 1);
        } catch (Exception ep) {

        }
        return null;
    }

    public double getVelocity(final double slope, final double power) {
        double v = 20;
        double prev = 0;
        while (Math.abs(v - prev) > 0.001) {
            prev = v;
            v = doNewton(v, slope, power);
        }
        return v;
    }
    
    private double calcForce(final double v, final double slope) {
        return dragCoeff * v * v * v + mg * v * (slope + 0.005);
    }

    private double doNewton(double v, double slope, double power) {
        double vv = v * v;
        double ans = calcForce(v, slope) - power;
        double adash = 3 * dragCoeff * vv + mg * (slope + 0.005);
        return v - ans / adash;
    }

    private Vector<Locat> loadRoute(final String routeName, final Vector<Locat> gpxData) {
        Vector<Locat> interpoledTrack = new Vector<>();
        int i = 1;
        Locat curr = null;
        while (i < gpxData.size()) {
            Locat prev  = gpxData.get(i-1);
            curr = gpxData.get(i);
            double dist = prev.distanceTo(curr);
            int seg = (int)(dist / 50.0);
            if (seg > 1) {
                System.out.println("Found " + seg + " segments with dist " + dist);
            }
            interpoledTrack.add(prev);
            Locat intLocPrev = prev;
            Locat intLocCurr = null;
            for (int j = 0; j < seg; j++) {
                intLocCurr = addSegmentToPoint(intLocPrev, curr);
                interpoledTrack.add(intLocCurr);
                intLocPrev = intLocCurr;
            }
            i++;
        }
        interpoledTrack.add(curr); // add the last one
        System.out.println("Interpolated track size: " + interpoledTrack.size());
        this.gpx = gpxData;
        Locat lstKnwnP = loadLastPosIfAvail();
        double minDist = Double.MAX_VALUE;
        Locat lastKnownPos = lstKnwnP;
        for (int i = 0; i < gpx.size(); i++) {
            double ddd = gpx.get(i).distanceTo(lstKnwnP);
            if (ddd < minDist) {
                minDist = ddd;
                lastKnownPos = gpx.get(i);
            }
        }
        totalDistance = 0;
        if (lastKnownPos != null) {
            current = lastKnownPos;
            int ilp = gpx.indexOf(current);
            next = gpx.get(ilp+1);
            try {
                for (i = 1; i < gpx.indexOf(lastKnownPos); i++) {
                    totalDistance += gpx.get(i).distanceTo(gpx.get(i - 1));
                }
            }catch (Exception e) {

            }
            System.out.println("Resuming track at GPS point: " + lastKnownPos + " with index: " + ilp + " distance covered: " + totalDistance);
        } else {
            current = gpx.get(0);
            next = gpx.get(1);
        }
        for (i = 0; i < gpx.size(); i++) {
            if (gpx.get(i) != null) {
                requestedDownload.put(gpx.get(i), false);
            }
        }
        i = gpx.indexOf(current);
        if (i < 1) {
            i = 1;
        }
        for (; i < gpx.indexOf(current) + 5 && i < gpx.size(); i++) {
            precacheDownloadImage(gpx.get(i-1), gpx.get(i));
        }
        return gpx;
    }

    private void precacheDownloadImage(final Locat prev, final Locat curr) {
        if (locatToImages.get(prev) == null && requestedDownload.get(prev) == false) {
            if (prev != null && curr != null) {
                requestedDownload.put(prev, true);
                final int bearing = (int) Math.toDegrees(prev.bearingTo(curr));
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        downloadImage(prev, bearing);
                    }
                };
                executor.execute(runnable);
            }
        }
    }

    private Locat loadLastPosIfAvail() {
        try {
            String lst = FileUtils.readFileToString(new File(getLastPosition()));
            String[] items = lst.split(",");
            return new Locat(Double.parseDouble(items[1]), Double.parseDouble(items[2]));
            /*if (gpx.get(:qpos).distanceTo(tmp) < 300) {
                return gpx.get(pos);
            }*/
        } catch (Exception e) {
            System.out.println("No last known position");
            //e.printStackTrace();
        }
        return null;
    }

    private Locat addSegmentToPoint(Locat prev, Locat loc) {
        double bearing = prev.bearingTo(loc);
        Locat mid = prev.addDistanceToGps(prev.distanceTo(loc)/2, bearing);
        /*var Bx = Math.cos(φ2) * Math.cos(λ2-λ1);
        var By = Math.cos(φ2) * Math.sin(λ2-λ1);
        var φ3 = Math.atan2(Math.sin(φ1) + Math.sin(φ2),
                Math.sqrt( (Math.cos(φ1)+Bx)*(Math.cos(φ1)+Bx) + By*By ) );
        var λ3 = λ1 + Math.atan2(By, Math.cos(φ1) + Bx);*/
        return mid;
    }

    private void displayImage(final Locat loc) {
        long start = System.currentTimeMillis();
        BufferedImage get = locatToImages.get(loc);
        if (get != null) {
            //Image resized = resizeImage(get);
            imgLbl.setIcon(new ImageIcon(get));
        }
        int indx = gpx.indexOf(loc)-1;
        for (int i = indx; i > 0; i--) {
            if (locatToImages.get(gpx.get(i)) != null) {
                locatToImages.remove(gpx.get(i));
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("Time taken to display image: " + (end - start) + " ms");
    }

    private Image resizeImage(final BufferedImage img) {
        return img.getScaledInstance(640 * 2, 640 * 2, Image.SCALE_DEFAULT);
    }

    private void saveFile(final String fileString, final BufferedImage image) {
        try {
            File file = new File("lcoation");
            ImageIO.write(image, "png", file);
        } catch (Exception ep) {
            logger.error("", ep);
        }
    }

    private void downloadImage(final Locat loc, final int bearing) {
        if (disableImagery) {
            return;
        }
        String urlStr = "";
        try {
            /*String fileString = loc.getLatitude() + loc.getLongitude() + bearing + ".png";
            File file = new File(fileString);
            if (file.exists()) {
                BufferedImage image = ImageIO.read(file);
                locatToImages.put(loc, image);
            } else {*/
            if (loc == null) {
                return;
            }
            urlStr = "https://maps.googleapis.com/maps/api/streetview?size=640x640&location=" + loc.getLatitude() + "," + loc.getLongitude() + "&fov=90&heading=" + bearing + "&pitch=10&key=AIzaSyDjRaMZV-KKG2jsl0unGYCH6SGoVqB2E-o";
            URL url = new URL(urlStr);
            BufferedImage image = ImageIO.read(url);
            locatToImages.put(loc, image);
            //saveFile(fileString, image);
            //}
        } catch (final IOException e) {
            logger.error("Error downloading file: " + urlStr);
        }
    }

    @Override
    public Vector<Locat> getGpxRoute() {
        return this.gpx;
    }

    @Override
    public double getTotalDistance() {
        return totalDistance;
    }

    @Override
    public Vector<Locat> getUserRide() {
        return userActivity;
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        try {
            Locat one = new Locat(36.12, -86.67);
            Locat two = new Locat(33.94, -118.40);
            System.out.println("Testing distance calc 2887: " + one.distanceTo(two));
            System.out.println("Testing distance calc: " + two.distanceTo(one));
            final String route = args[0];
            final File file = new File(route);
            final String name = route.substring(route.lastIndexOf("/")+1, route.length());
            final Vector<Locat> gpx = new Vector(GpxReader.getPoints(file));
            System.out.println("Initial size: " + gpx.size());
            final File userRide = new File("/home/dylan/Dropbox/Dylan/Cycling/RidesVirtual/"+name+System.currentTimeMillis()+".gpx");
            final VirtualCycler vc = new VirtualCycler(8088, name, gpx, userRide);
            System.out.println("Ready...");
            vc.start();
            /*for (int i = 0 ; i < 5 ;i++) {
                long nn = System.currentTimeMillis();
                vc.updateAvhHr(nn, 100);
                vc.update(nn);
                Thread.sleep(10000);
            }
            List<String> lln = new ArrayList<>();
            for (int hb = 0; hb < 200; hb++) {
                lln.add(""+hb + "," + vc.getPower(hb));
            }
            FileUtils.writeLines(new File("heartmap"), lln);*/
        } catch (Exception ep) {
            ep.printStackTrace();
        }
    }

    @Override
    protected void downstreamOnMessage(WebSocket webSocket, WebsocketMessage websocketMessage) {
        System.out.println("downstreamOnMessage: " + gson.toJson(websocketMessage));
        process(websocketMessage);
    }

    @Override
    protected void upstreamOnMessage(WebSocket webSocket, WebsocketMessage websocketMessage) {
        System.out.println("upStream: " + gson.toJson(websocketMessage));
        process(websocketMessage);
    }

    @Override
    protected String getInstanceName() {
        return "VitualClcerApp";
    }
}
