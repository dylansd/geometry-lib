package com.dksd.cycling.common.activity;

import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.math.stat.StatUtils;

import com.dksd.cycling.common.Definitions;
import com.dksd.cycling.common.point.RidePoint;
import com.dksd.cycling.common.zones.Zone;
import com.dksd.cycling.common.zones.Zones;

public class IntervalImpl implements Interval {

	private static int id;
	private final List<RidePoint> points;
	private double meanPower = -1;
	private double variancePower = -1;
	private final int start;
	private final int end;
	private final int ftp;
	private final int identifier = id++;	 

	public IntervalImpl(List<RidePoint> points, int start, int end, int ftp) {
		// this.intervalDef = intervalDef;
		this.points = points;
		this.start = start;
		this.end = end;
		this.ftp = ftp;
	}

	public IntervalImpl(Interval before, Interval adjoined, int ftp) {
		// this.intervalDef = before.getIntervalLengthSecs();
		this.start = before.getStart();
		this.end = adjoined.getEnd();
		this.points = before.getRidePoints();
		this.ftp = ftp;
	}

	private double[] getDoubleForPower(int start, int end) {
		int size = end - start;
		double[] ret = new double[size];
		for (int i = 0; i < size; i++) {
			if (i + start >= points.size()) {
				return ret;
			}
			ret[i] = points.get(i + start).getPower();
		}
		return ret;
	}

	public String toString() {
		int displayTime = getIntervalLengthSecs();
		String units = " secs";
		if (getIntervalLengthSecs() > 60) {
			displayTime = getIntervalLengthSecs() / 60;
			units = " min";
		}		
        DecimalFormat df = new DecimalFormat("#.##");
        String xc =  " Start GPS: " + Definitions.semicircleToDegrees(points.get(getStart()).getX()) + ","+Definitions.semicircleToDegrees(points.get(getStart()).getY()); 
        String ec =  " End GPS: " + Definitions.semicircleToDegrees(points.get(getEnd()).getX()) + ","+Definitions.semicircleToDegrees(points.get(getEnd()).getY());
        
		return "" + displayTime + units + " @ " + (int) getMeanPower()
				+ " Watts" + " TimeRange=[" + start + ":" + end + "]"
				+ " TSS: " + df.format(getTrainingStressScore()) + " IF: " + df.format((getMeanPower()/ftp)) + xc + ec;
		}

	@Override
	public int getIntervalLengthSecs() {
		return end - start;
	}

	@Override
	public double getMeanPower() {
		if (meanPower < 0) {
			meanPower = calcPower();
		}
		return meanPower;
	}

	private double calcPower() {
		return StatUtils.mean(getDoubleForPower(start, end));
	}

	@Override
	public double getVariancePower() {
		if (variancePower < 0) {
			variancePower = calcVar();
		}
		return variancePower;
	}

	private double calcVar() {
		return StatUtils.variance(getDoubleForPower(start, end));
	}

	@Override
	public int getStart() {
		return start;
	}

	@Override
	public int getEnd() {
		return end;
	}

	@Override
	public List<RidePoint> getRidePoints() {
		return points;
	}

	@Override
	public Zone getZone(Zones zones) {
		for (Zone z : zones.getZones()) {
			if (inZone(z)) {
				return z;
			}
		}
		return null;
	}

	private boolean inZone(Zone zone) {
		double ofFtp = getMeanPower() / (double)ftp;
		if (ofFtp >= zone.getStartPower() && ofFtp <= zone.getEndPower()) {
			return true;
		}
		return false;
	}

	@Override
	public int getIdentifier() {
		return identifier;
	}

	@Override
	public double getTrainingStressScore() {
		double NP = (double)getMeanPower();
		double IF = NP / (double)ftp;
		return 100 * NP * (double)getIntervalLengthSecs() * IF / ((double)ftp * 3600.0);
	}

}
