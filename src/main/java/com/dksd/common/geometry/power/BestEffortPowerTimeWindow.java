package com.dksd.cycling.strava.window;

import java.util.LinkedList;

import com.dksd.cycling.strava.common.Definitions;
import com.dksd.cycling.strava.common.PowerDefinitions;
import com.dksd.cycling.strava.point.RidePoint;
import com.dksd.cycling.strava.pointlist.measurable.DistributionResult;
import com.dksd.cycling.strava.realworld.Human;
import com.dksd.cycling.strava.realworld.Transport;
import com.dksd.cycling.strava.realworld.Weather;

/**
 * @author Dylan Scott-Dawkins
 */
public class BestEffortPowerTimeWindow extends AbstractTimeWindow {

   private final Human human;
   private final Weather weather;
   private final Transport transport;
   private double power = -1;

   /**
    * @param timeWindow time window
    */
   public BestEffortPowerTimeWindow(Weather weather, Human human, Transport transport, int timeWindow) {
      super(timeWindow);
      this.human = human;
      this.weather = weather;
      this.transport = transport;
   }

   @Override
   public void performCalculation(LinkedList<RidePoint> points, RidePoint prevObject, RidePoint object) {
      /*		double v = getTotalDistanceSoFar() / getTotalTimeSoFar();
      		double s = getElevationGainSoFar() / getTotalDistanceSoFar();
      		double result = (Definitions.EARTH_GRAVITY
      				* (human.getWeight() + transport.getWeight()) * v * s);
      		if (result > power) {
      			power = result;
      		}
      		return;*/

      double windSpeed = weather.getWindSpeed();
      double windDirection = Math.toRadians(weather.getWindDirection());
      double rho_air_density = weather.getAirDensity();
      double riderDirection = prevObject.getBearingTo(object);
      double grade = getElevationGainSoFar() / getTotalDistanceSoFar();
      double initVelocity = Definitions.getVelocity(points.get(getStartIndex()), points.get(getStartIndex() + 1));
      double finalVelocity = Definitions.getVelocity(prevObject, object);
      double velGround = getTotalDistanceSoFar() / getTotalTimeSoFar();
      double velWtan = windSpeed * Math.cos(riderDirection - windDirection);
      double va = velGround + velWtan; 
      double v_wnor = windSpeed * Math.sin(riderDirection - windDirection); 
      double yaw =  Math.atan(v_wnor / va); //used in calculating cda
      double dragOfSpokes = PowerDefinitions.DRAG_OF_SPOKES;
      double gravity = 9.81;
      double pat = va * va * velGround / 2 * rho_air_density * (PowerDefinitions.CDA + dragOfSpokes);
      double prr = velGround * Math.cos(Math.atan(grade)) * PowerDefinitions.ROLLING_RESISTANCE
            * (human.getWeight() + transport.getWeight()) * gravity;
      double pwb = velGround * (91 + 8.7 * velGround) * 0.001;
      double ppe = velGround * (transport.getWeight() + human.getWeight()) * gravity * Math.sin(Math.atan(grade));
      double pke1 = 0.5 * (transport.getWeight() + human.getWeight() + PowerDefinitions.INERTIA
            / (PowerDefinitions.RADIUS * PowerDefinitions.RADIUS));
      double pke2 = (finalVelocity * finalVelocity - initVelocity * initVelocity) / getTotalTimeSoFar();
      double pke = pke1 * pke2;
      double total = pke + ppe + pwb + prr + pat;
      if (total > power) {
         power = total;
      }

   }

   public double getCalories() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getAvgHR() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getEnergy() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getAvgPower() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getMaxSpeed() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getAverageSpeed() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getAvgCadence() {
      // TODO Auto-generated method stub
      return 0;
   }

   public double getPower() {
      return power;
   }

@Override
public DistributionResult getVelocityDistribution() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DistributionResult getCadenceDistribution() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public DistributionResult getHeartRateDistribution() {
	// TODO Auto-generated method stub
	return null;
}

}
