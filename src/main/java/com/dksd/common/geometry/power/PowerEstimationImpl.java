package com.dksd.cycling.garmin.estimator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dksd.cycling.common.Definitions;
import com.dksd.cycling.garmin.fit.UserProfile;
import com.dksd.cycling.services.WeatherService;
import com.dksd.cycling.strava.common.PowerDefinitions;
import com.garmin.fit.Mesg;
import com.garmin.fit.RecordMesg;

public class PowerEstimationImpl implements Estimator<Mesg> {

    private final static Logger logger = LoggerFactory.getLogger(PowerEstimationImpl.class);
    private final WeatherService weatherService;
    private Random rand = new Random();

    public PowerEstimationImpl(final WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @Override
    public boolean estimate(final List<Mesg> messagesIn, final UserProfile userProfile, final Map<Long, Double> timestampToEstPower, boolean printDebug)
            throws Exception {
        boolean isRecPower = false;
        timestampToEstPower.clear();
        List<RecordMesg> messages = new ArrayList<RecordMesg>();
        double elevationGain = 0.0;
        RecordMesg prevMsg = null;
        for (int i = 0; i < messagesIn.size(); i++) {
            if (messagesIn.get(i) instanceof RecordMesg) {
                RecordMesg rm = (RecordMesg) messagesIn.get(i);
                messages.add(rm);
                if (prevMsg == null) {
                    prevMsg = rm;
                } else {
                    elevationGain += rm.getAltitude().floatValue() - prevMsg.getAltitude().floatValue();
                }
            }
        }
        if (Math.abs(elevationGain) <= 1.0) {
            // Probably a turbo trainer session?
            // throw new Exception("Turbo trainer session detected!");
        }
        // this is used to query the mid point of the ride for weather.
        // Hopefully this will result in less weather API calls
        if (messages.size() > 5) {
            weatherService.getWindSpeed(messages.get(messages.size() / 2));
        }

        for (int i = 0; i < messages.size(); i++) {
            RecordMesg curr = messages.get(i);
            Short cad = curr.getCadence();
            short safecad = 90;
            Integer recordedPower = curr.getPower();
            boolean invalidPowerEstimated = false;
            double total = 0;
            if (cad != null) {
                safecad = cad.shortValue();
            }
            if (safecad > 80) {
                double windSpeed = weatherService.getWindSpeed(curr);
                double windDirection = Math.toRadians(weatherService.getWindDirection(curr));
                double airDensity = weatherService.getAirDensity(curr);
                int temperature = weatherService.getTemperature(curr);
                double grad = getGrad(messages, i, userProfile.getGradientWindowSize());
                try {
                    total = doPowerCalc(userProfile, messages, i, userProfile.getPowerWindow(), windSpeed, windDirection, temperature, airDensity, safecad,
                            grad, calcRiderDirection(messages, i, userProfile.getGradientWindowSize()));
                } catch (InvalidPowerException ipe) {
                    invalidPowerEstimated = true;
                } catch (Exception ep) {
                    ep.printStackTrace();
                    System.exit(1);
                }
            } else {
                total = 0;
            }
            if (!invalidPowerEstimated) {
                int finalPower = (int) total;
                if (recordedPower == null) {
                    if (curr != null) {
                        curr.setPower(finalPower);
                    }
                } else {
                    isRecPower = true;
                    if (recordedPower.intValue() > 100 && safecad >= 80) {
                        timestampToEstPower.put(curr.getTimestamp().getTimestamp(), total);
                        if (printDebug) {
                           logger.info("Cadence: " + safecad + ", Actual power: " + recordedPower.intValue() + " Estimated power: " + finalPower);
                       }
                    }
                }
            }
        }
        return isRecPower;
    }

    private double getGrad(List<RecordMesg> messages, int i, int gradientWindowSize) {
	   //Here i should only take points that fallen on a straight line behind me...
	   //check the start point with start - 1 for direction vs current direction...
	   
      try {
         RecordMesg prev = getPrev(messages, i, gradientWindowSize);
         RecordMesg prevMinusOne = getPrev(messages, i, gradientWindowSize-1);
         RecordMesg next = getNext(messages, i, gradientWindowSize);
         RecordMesg nextMinusOne = getNext(messages, i, gradientWindowSize-1);
         double startDirection = getDirection(prev, prevMinusOne);
         double endDirection = getDirection(nextMinusOne, next);
         //comparing these is going to be tough... better to have metrics like below..
         double dist = next.getDistance().floatValue() - prev.getDistance().floatValue();
         if (dist == 0) {
            return 0;
         }
         double eleOrig = next.getAltitude().floatValue() - prev.getAltitude().floatValue();
         double barom = eleOrig / dist;
         return barom;
      } catch (NullPointerException npe) {
         return 0;
      }
    }

    private RecordMesg getPrev(List<RecordMesg> messages, int i, int gradientWindowSize) {
        if (i - gradientWindowSize < 0) {
            return messages.get(0);
        }
        if (i - gradientWindowSize >= messages.size()) {
            return messages.get(messages.size() - 1);
        }
        return messages.get(i - gradientWindowSize);
    }

    private RecordMesg getNext(List<RecordMesg> messages, int i, int gradientWindowSize) {
        if (i + gradientWindowSize >= messages.size()) {
            return messages.get(messages.size() - 1);
        }
        if (i + gradientWindowSize < 0) {
            return messages.get(0);
        }
        return messages.get(i + gradientWindowSize);
    }

    private double calcRiderDirection(List<RecordMesg> messages, int i, int gradientWindowSize) {
        RecordMesg prev = getPrev(messages, i, gradientWindowSize);
        RecordMesg next = getNext(messages, i, gradientWindowSize);
        return getDirection(prev, next);
    }

    private double getDirection(RecordMesg prev, RecordMesg next) {
        double lat1 = Definitions.semicircleToDegrees(prev.getPositionLat());
        double lon1 = Definitions.semicircleToDegrees(prev.getPositionLong());
        double lat2 = Definitions.semicircleToDegrees(next.getPositionLat());
        double lon2 = Definitions.semicircleToDegrees(next.getPositionLong());
        return Definitions.getRiderDirection(lat1, lon1, lat2, lon2);
    }

    private double doPowerCalc(final UserProfile userProfile, List<RecordMesg> messages, int i, int powerWindow, double windSpeedKph, double windDirection,
            int temperature, double rho_air_density, short cadence, double grade, double riderDirection) throws InvalidPowerException {
        RecordMesg curr = messages.get(i);
        if (curr.getPositionLat() == null || curr.getPositionLong() == null || curr.getSpeed() == null) {
            throw new InvalidPowerException("Current GPS Position unavailable to calculate power: " + curr);
        }
        RecordMesg next = getNext(messages, i, powerWindow);
        RecordMesg prev = getPrev(messages, i, powerWindow);
        if ((next.getPositionLat().intValue() - prev.getPositionLat().intValue()) == 0
                && (next.getPositionLong().intValue() - prev.getPositionLong().intValue() == 0)) {
            throw new InvalidPowerException("GPS Position unavailable to calculate power, is this a TURBO session? : " + curr);
        }
        double velGround = getGroundSpeed(prev, curr, next);
        if (velGround < 1.4) {
            return 0;
        }
        double velApparent = getApparentvel(userProfile, windSpeedKph, windDirection, riderDirection, velGround);
        double simplePower = calcSimplePower(userProfile, grade, velGround, velApparent, rho_air_density);
        double complexPower = calcComplexPower(userProfile, prev, curr, grade, velGround, velApparent, rho_air_density);
        double total = complexPower;
        if (total < 0) {
            return 0;
        }
        if (total > 600) {
           return 400 + randomEitherSide(10);
        }
        return total;
    }

    private int randomEitherSide(int n) {
       if (rand.nextBoolean())
          return rand.nextInt(n);
       else 
          return -rand.nextInt(n);
   }

   private double calcComplexPower(UserProfile userProfile, RecordMesg prev, RecordMesg curr, double grade, double velGround, double velApparent, double rho_air_density) {
       double initVelocity = prev.getSpeed().floatValue();
       double finalVelocity = curr.getSpeed().floatValue();
       double time = curr.getTimestamp().getTimestamp() - prev.getTimestamp().getTimestamp();
       double cda = userProfile.getDragOfRiderAndBicycle();
       double dragOfSpokes = userProfile.getDragOfWheels();
       double pat = velApparent * velApparent * velGround / 2 * rho_air_density * (cda + dragOfSpokes);
       double prr = velGround * PowerDefinitions.ROLLING_RESISTANCE
             * (userProfile.getHuman().getWeight() + userProfile.getTransport().getWeight()) * Definitions.EARTH_GRAVITY;
       double pwb = velGround * (91 + 8.7 * velGround) * 0.001;
       double ppe = velGround * (userProfile.getTransport().getWeight() + userProfile.getHuman().getWeight()) * Definitions.EARTH_GRAVITY
             * grade;
       double pke1 = 0.5 * (userProfile.getTransport().getWeight() + userProfile.getHuman().getWeight() + PowerDefinitions.INERTIA
             / (PowerDefinitions.RADIUS * PowerDefinitions.RADIUS));
       double pke2 = (finalVelocity * finalVelocity - initVelocity * initVelocity) / time;
       double pke = pke1 * pke2;
       return (pke + ppe + pwb + prr + pat) / 0.976;
   }

   private double calcSimplePower(UserProfile userProfile, double grade, double velGround, double velApparent, double rho_air_density) {
        double k1 = 0.0055;
        double k2 = userProfile.getDragOfRiderAndBicycle();
        double weight = (userProfile.getTransport().getWeight() + userProfile.getHuman().getWeight());
        return weight * Definitions.EARTH_GRAVITY * velGround * (k1 + grade) + k2 * velApparent * velApparent * velGround * rho_air_density;
    }

    private double getApparentvel(UserProfile userProfile, double windSpeedKph, double windDirection, double riderDirection, double velGround) {
        double windSpeed = (windSpeedKph / 3.6) * userProfile.getWindReductionFactor();
        if (windSpeed < 0) {
            windSpeed = 0;
        }
        double velWtan = windSpeed * Math.cos(windDirection - riderDirection);
        return velGround + velWtan;
    }

    private double getGroundSpeed(RecordMesg prev, RecordMesg curr, RecordMesg next) {
        double velGround = curr.getSpeed().floatValue();
        double velGroundAvg = -1;
        try {
            velGroundAvg = (next.getDistance().floatValue() - prev.getDistance().floatValue()) / (double) (next.getTimestamp().getTimestamp() - prev.getTimestamp().getTimestamp());
        } catch (NullPointerException npe) {
            velGroundAvg = velGround;
        }
        if (velGround > 10 && velGround > velGroundAvg && velGroundAvg / velGround < 0.5) {
            logger.error("We have a problem with inconsistent velocities... Average: " + velGroundAvg + " instant: " + velGround);
            velGround = velGroundAvg;
        }
        return velGround;
    }

}
