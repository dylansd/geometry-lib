package com.dksd.service.share;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Created by dylan on 8/20/16.
 */
@RestController
public class ExampleController  {

    private final Collection<EndpointNotifier> endpointNotifiers;

    @Autowired
    public ExampleController(Collection<EndpointNotifier> endpointNotifiers) {
        this.endpointNotifiers = endpointNotifiers;
    }

    @RequestMapping("ping")
    public String ping() {
        return "awake";
    }

    //https://dksd-locationshare.herokuapp.com/location
    @RequestMapping("location")
    public String update(@RequestParam(value = "from") String from,
                         @RequestParam(value = "to") String to,
                         @RequestParam(value = "subject") String subject,
                         @RequestParam(value = "message") String message) {
        try {
            Properties properties = new Properties();
            properties.setProperty("from", from);
            properties.setProperty("to", to);
            properties.setProperty("subject", subject);
            properties.setProperty("message", message);
            for (EndpointNotifier endpointNotifier : endpointNotifiers) {
                endpointNotifier.notify(properties);
            }
        } catch (Exception ep) {
            return "not ok";
        }
        return "result ok";
    }

    /**
     * Calculate the height of a tree.
     * If the ground before the tree is sloped then measure the angle to the base of the tree as the first parameter.
     * If no slope then enter 0.
     *
     * Next measure the angle to top of the tree.
     * finally measure the distance to the base of the tree.
     *
     * @param angle_tree_base
     * @param measurable_dist_to_tree_base
     * @param angle_to_top_of_tree
     * @return
     * https://dksd-locationshare.herokuapp.com/treeHeight
     */
    @RequestMapping("treeHeight")
    public Long treeHeight(@RequestParam(value = "angle_to_tree_base") Double angle_tree_base,
                              @RequestParam(value = "distance_to_tree_base") Double measurable_dist_to_tree_base,
                              @RequestParam(value = "angle_to_top_of_tree") Double angle_to_top_of_tree) {
        double horizontalDistToTreeBase = measurable_dist_to_tree_base * Math.cos(Math.toRadians(angle_tree_base));
        return Math.round(horizontalDistToTreeBase * Math.tan(Math.toRadians(angle_to_top_of_tree)));
    }

    /**
     * Calculate the velocity in km/h from KV rating and diameter of brushless outrunner motor
     *
     * @param kiloVolts kilo brah
     * @param diameter diamtere
     * @param
     * @return
     * https://dksd-locationshare.herokuapp.com/outrunnerVelocity?kv=750&diamm=28&voltage=7.4
     */
    @RequestMapping("outrunnerVelocity")
    public Double outrunnerVelocity(@RequestParam(value = "kv") Double kiloVoltRating,
                           @RequestParam(value = "diamm") Double diameter,
                           @RequestParam(value = "voltage") Double voltage) {
            double rpsecond = kiloVoltRating / 60.0;
            double motorDistancePerSecond = diameter * 2.0 * Math.PI / 1000.0;
            return voltage * rpsecond * motorDistancePerSecond * 3.6;
    }


    /**
     * Calculate power required to travel at a certain speed
     * 300 watts @ 11m/s rider 83% will be air resistance.
     *
     * @param vms speed
     * @param cda cross sectional diameter constant, about 0.5-0.3
     * @return
     * https://dksd-locationshare.herokuapp.com/bicyclePower?vms=11&cda=0.4&totalMass=90&slope=0
     */
    @RequestMapping("bicyclePower")
    public Double bicyclePower(@RequestParam(value = "vms") Double vms,
                               @RequestParam(value = "cda") Double cda,
                               @RequestParam(value = "totalMass") Double totalMass,
                               @RequestParam(value = "slope") Double slope) {
        double driveEfficiency = 0.98;
        return (getPowerDrag(vms, cda)
                + getPowerRolling(vms, totalMass)
                + getPowerClimbing(vms, totalMass, slope)) / driveEfficiency;
    }

    //where is the JSON example that I like?
    //This is a bit too basic..
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<String> sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        return new ArrayList();
    }

    /**
     * Calculate power required to travel at a certain speed
     * 300 watts @ 11m/s rider 83% will be air resistance.
     *
     * @param vms speed
     * @param cda cross sectional diameter constant, about 0.5-0.3
     * @return
     * https://dksd-locationshare.herokuapp.com/saveGpxActivity?vms=11&cda=0.4&totalMass=90&slope=0
     * Activity name
     * time
     *
     */
    /*
    TODO has to be a POST with body content.
    @RequestMapping("saveGpxActivity")
    public Double saveGpxActivity(@RequestParam(value = "name") String name,
                               @RequestParam(value = "cda") Double cda,
                               @RequestParam(value = "totalMass") Double totalMass,
                               @RequestParam(value = "slope") Double slope) {
        double driveEfficiency = 0.98;
        return (getPowerDrag(vms, cda)
                + getPowerRolling(vms, totalMass)
                + getPowerClimbing(vms, totalMass, slope)) / driveEfficiency;
    }*/



    /*
Wire Circular Mils =	(Conductor Resistivity)(2)(Amps)(One Way Distance in Feet)
Allowable Voltage Drop
For 3-phase circuits:
Wire Circular Mils =	(Conductor Resistivity)(2)(Amps)(One Way Distance in Feet)(.866)
Allowable Voltage Drop

     */

    /* Sun position

     */



}

