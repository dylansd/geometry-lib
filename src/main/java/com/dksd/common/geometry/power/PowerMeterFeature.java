package com.cycling.science.trainwithscience.feature;

import android.content.Context;
import android.util.Log;

import com.cycling.science.trainwithscience.DataField;
import com.cycling.science.trainwithscience.logging.ALoggerFactory;
import com.cycling.science.trainwithscience.writer.DataWriter;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc;
import com.dsi.ant.plugins.antplus.pcc.defines.BatteryStatus;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPlusCommonPcc;
import com.dsi.ant.plugins.antplus.pccbase.AsyncScanController;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * Created by dscottdawkins on 5/1/17.
 */

public class PowerMeterFeature extends AbstractAntFeature<AntPlusBikePowerPcc> {

    private final Context context;
    //NOTE: We're using 2.07m as the wheel circumference to pass to the calculated events
    BigDecimal wheelCircumferenceInMeters = new BigDecimal("2.07");

    public PowerMeterFeature(Context context, DataWriter dataWriter, ALoggerFactory aLoggerFactory) {
        super("PowerMeterFeature", dataWriter, aLoggerFactory);
        this.context = context;
    }

    /**
     * Requests the asynchronous scan controller
     */
    protected void requestAccessToPcc() {
        Log.i(getName(), "Async scanning for power meters!");
        super.runOnLooperThread("PowerLooper", new Runnable() {
            @Override
            public void run() {
                asyncScanController = AntPlusBikePowerPcc.requestAsyncScanController(context, 0,
                        new AsyncScanController.IAsyncScanResultReceiver() {
                            @Override
                            public void onSearchStopped(RequestAccessResult reasonStopped) {
                                asyncSearchStopped(reasonStopped);
                            }

                            @Override
                            public void onSearchResult(AsyncScanController.AsyncScanResultDeviceInfo deviceFound) {
                                asyncSearchResult(deviceFound);
                            }
                        });
            }
        });
    }

    protected void subscribeToEvents() {
            requestCalibration();
            Log.i(getName(), "Subscribing to power meter events");
            antPluginPcc.subscribeCalculatedPowerEvent(new AntPlusBikePowerPcc.ICalculatedPowerReceiver() {
                @Override
                public void onNewCalculatedPower(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final BigDecimal calculatedPower) {
                    getDataWriter().addDataPoint(DataField.PWR_CALC_POWER, estTimestamp, calculatedPower);
                }
            });

            antPluginPcc.subscribeCalculatedTorqueEvent(new AntPlusBikePowerPcc.ICalculatedTorqueReceiver() {
                @Override
                public void onNewCalculatedTorque(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final BigDecimal calculatedTorque) {
                    getDataWriter().addDataPoint(DataField.PWR_CALC_TORQUE, estTimestamp, calculatedTorque);
                }
            });

            antPluginPcc.subscribeCalculatedCrankCadenceEvent(new AntPlusBikePowerPcc.ICalculatedCrankCadenceReceiver() {
                @Override
                public void onNewCalculatedCrankCadence(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final BigDecimal calculatedCrankCadence) {
                    getDataWriter().addDataPoint(DataField.PWR_CALC_CRANK_CADENCE, estTimestamp, calculatedCrankCadence);
                }
            });

            antPluginPcc.subscribeCalculatedWheelSpeedEvent(new AntPlusBikePowerPcc.CalculatedWheelSpeedReceiver(
                    wheelCircumferenceInMeters) {
                @Override
                public void onNewCalculatedWheelSpeed(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final BigDecimal calculatedWheelSpeed) {
                    getDataWriter().addDataPoint(DataField.PWR_CALC_WHEEL_SPEED, estTimestamp, calculatedWheelSpeed);
                }
            });

            antPluginPcc.subscribeCalculatedWheelDistanceEvent(new AntPlusBikePowerPcc.CalculatedWheelDistanceReceiver(
                    wheelCircumferenceInMeters) {
                @Override
                public void onNewCalculatedWheelDistance(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final BigDecimal calculatedWheelDistance) {
                    getDataWriter().addDataPoint(DataField.PWR_CALC_WHEEL_DISTANCE, estTimestamp, calculatedWheelDistance);
                }
            });

            antPluginPcc.subscribeInstantaneousCadenceEvent(new AntPlusBikePowerPcc.IInstantaneousCadenceReceiver() {
                @Override
                public void onNewInstantaneousCadence(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.DataSource dataSource,
                        final int instantaneousCadence) {
                    getDataWriter().addDataPoint(DataField.PWR_CADENCE, estTimestamp, instantaneousCadence);
                }
            });

            antPluginPcc.subscribeRawPowerOnlyDataEvent(new AntPlusBikePowerPcc.IRawPowerOnlyDataReceiver() {
                @Override
                public void onNewRawPowerOnlyData(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long powerOnlyUpdateEventCount,
                        final int instantaneousPower,
                        final long accumulatedPower) {
                    getDataWriter().addDataPoint(DataField.POWER, estTimestamp, instantaneousPower);
                    getDataWriter().addDataPoint(DataField.PWR_UPDATE_EVENT_COUNT, estTimestamp, powerOnlyUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_POWER, estTimestamp, accumulatedPower);
                }
            });

            antPluginPcc.subscribePedalPowerBalanceEvent(new AntPlusBikePowerPcc.IPedalPowerBalanceReceiver() {
                @Override
                public void onNewPedalPowerBalance(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final boolean rightPedalIndicator,
                        final int pedalPowerPercentage) {
                    if (rightPedalIndicator) {
                        getDataWriter().addDataPoint(DataField.PWR_RIGHT_PEDAL_PERCENTAGE, estTimestamp, pedalPowerPercentage);
                    } else {
                        getDataWriter().addDataPoint(DataField.PWR_LEFT_PEDAL_PERCENTAGE, estTimestamp, pedalPowerPercentage);
                    }
                }
            });

            antPluginPcc.subscribeRawWheelTorqueDataEvent(new AntPlusBikePowerPcc.IRawWheelTorqueDataReceiver() {
                @Override
                public void onNewRawWheelTorqueData(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long wheelTorqueUpdateEventCount,
                        final long accumulatedWheelTicks,
                        final BigDecimal accumulatedWheelPeriod,
                        final BigDecimal accumulatedWheelTorque) {
                    getDataWriter().addDataPoint(DataField.PWR_WHEEL_TORQ_EVNT_CNT, estTimestamp, wheelTorqueUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_WHEEL_TICKS, estTimestamp, accumulatedWheelTicks);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_WHEEL_PERIOD, estTimestamp, accumulatedWheelPeriod);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_WHEEL_TORQ, estTimestamp, accumulatedWheelTorque);
                }
            });

            antPluginPcc.subscribeRawCrankTorqueDataEvent(new AntPlusBikePowerPcc.IRawCrankTorqueDataReceiver() {
                @Override
                public void onNewRawCrankTorqueData(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long crankTorqueUpdateEventCount,
                        final long accumulatedCrankTicks,
                        final BigDecimal accumulatedCrankPeriod,
                        final BigDecimal accumulatedCrankTorque) {
                    getDataWriter().addDataPoint(DataField.PWR_CRANK_TORQ_EVNT_CNT, estTimestamp, crankTorqueUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_CRANK_TICKS, estTimestamp, accumulatedCrankTicks);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_CRANK_PERIOD, estTimestamp, accumulatedCrankPeriod);
                    getDataWriter().addDataPoint(DataField.PWR_ACCUM_CRANK_TORQ, estTimestamp, accumulatedCrankTorque);
                }
            });

            antPluginPcc.subscribeTorqueEffectivenessEvent(new AntPlusBikePowerPcc.ITorqueEffectivenessReceiver() {
                @Override
                public void onNewTorqueEffectiveness(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long powerOnlyUpdateEventCount,
                        final BigDecimal leftTorqueEffectiveness,
                        final BigDecimal rightTorqueEffectiveness) {
                    getDataWriter().addDataPoint(DataField.PWR_ONLY_EVNT_CNT, estTimestamp, powerOnlyUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_LEFT_TORQ_EFFECTIVENESS, estTimestamp, leftTorqueEffectiveness);
                    getDataWriter().addDataPoint(DataField.PWR_RIGHT_TORQ_EFFECTIVENESS, estTimestamp, rightTorqueEffectiveness);
                }
            });

            antPluginPcc.subscribePedalSmoothnessEvent(new AntPlusBikePowerPcc.IPedalSmoothnessReceiver() {
                @Override
                public void onNewPedalSmoothness(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final long powerOnlyUpdateEventCount,
                        final boolean separatePedalSmoothnessSupport,
                        final BigDecimal leftOrCombinedPedalSmoothness,
                        final BigDecimal rightPedalSmoothness) {
                    getDataWriter().addDataPoint(DataField.PWR_ONLY_EVNT_CNT, estTimestamp, powerOnlyUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_SEPARATE_SMOOTHNESS_SUPPORT, estTimestamp, separatePedalSmoothnessSupport);
                    getDataWriter().addDataPoint(DataField.PWR_LEFT_OR_COMBINED_SMOOTHNESS, estTimestamp, leftOrCombinedPedalSmoothness);
                    getDataWriter().addDataPoint(DataField.PWR_RIGHT_PEDAL_SMOOTHNESS, estTimestamp, rightPedalSmoothness);
                }
            });

            antPluginPcc.subscribeRawCtfDataEvent(new AntPlusBikePowerPcc.IRawCtfDataReceiver() {
                @Override
                public void onNewRawCtfData(final long estTimestamp,
                                            final EnumSet<EventFlag> eventFlags,
                                            final long ctfUpdateEventCount,
                                            final BigDecimal instantaneousSlope,
                                            final BigDecimal accumulatedTimeStamp,
                                            final long accumulatedTorqueTicksStamp) {
                    getDataWriter().addDataPoint(DataField.PWR_CTF_UPDATE_EVNT_CNT, estTimestamp, ctfUpdateEventCount);
                    getDataWriter().addDataPoint(DataField.PWR_INSTANT_SLOPE, estTimestamp, instantaneousSlope);
                    getDataWriter().addDataPoint(DataField.PWR_ACC_TIMESTAMP, estTimestamp, accumulatedTimeStamp);
                    getDataWriter().addDataPoint(DataField.PWR_ACC_TORQ_TICKSSTAMP, estTimestamp, accumulatedTorqueTicksStamp);
                }
            });

            antPluginPcc.subscribeCalibrationMessageEvent(new AntPlusBikePowerPcc.ICalibrationMessageReceiver() {
                @Override
                public void onNewCalibrationMessage(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.CalibrationMessage calibrationMessage) {
                    try {
                        if (calibrationMessage.calibrationData != null) {
                            getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_MSG_DATA, estTimestamp, calibrationMessage.calibrationData);
                        }
                        if (calibrationMessage.ctfOffset != null) {
                            getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_MSG_CTFOFFSET, estTimestamp, calibrationMessage.ctfOffset);
                        }
                        if (calibrationMessage.manufacturerSpecificData != null) {
                            //getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_MANU_DATA, estTimestamp, calibrationMessage.manufacturerSpecificData);
                            int i =0 ;
                            for (byte b : calibrationMessage.manufacturerSpecificData) {
                                Log.i(getName(), "Manu specific data byte "+i+": " + b);
                                i++;
                            }
                        }
                        getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_MSG_CONTENTS, estTimestamp, calibrationMessage.describeContents());

                        if (calibrationMessage.calibrationId != null) {
                            getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_MSG_CALID, estTimestamp, calibrationMessage.calibrationId.toString());
                        }
                    } catch (Exception ep) {
                        Log.e(getName(), "Calibrations messages not working");
                    }
                }
            });

            antPluginPcc.subscribeAutoZeroStatusEvent(new AntPlusBikePowerPcc.IAutoZeroStatusReceiver() {
                @Override
                public void onNewAutoZeroStatus(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.AutoZeroStatus autoZeroStatus) {
                    getDataWriter().addDataPoint(DataField.PWR_AUTOZERO_STATUS, estTimestamp, autoZeroStatus.toString());
                }
            });

            antPluginPcc.subscribeMeasurementOutputDataEvent(new AntPlusBikePowerPcc.IMeasurementOutputDataReceiver() {
                @Override
                public void onNewMeasurementOutputData(long estTimestamp, EnumSet<EventFlag> enumSet, int numOfDataTypes, int dataType, BigDecimal timestmap, BigDecimal measurementValue) {
                    getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_OUTPUT_DATA_NUMOFTYPES, estTimestamp, numOfDataTypes);
                    getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_OUTPUT_DATA_DATATYPE, estTimestamp, dataType);
                    getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_OUTPUT_DATA_TIMESTAMP, estTimestamp, timestmap);
                    getDataWriter().addDataPoint(DataField.PWR_CALIBRATION_OUTPUT_DATA_VALUE, estTimestamp, measurementValue);
                }
            });

            /**Nt sure what RSSI evens means
             * Really need the documentation for this stuff
             */
            antPluginPcc.subscribeRssiEvent(new AntPlusCommonPcc.IRssiReceiver() {
                @Override
                public void onRssiData(long estTimestamp, EnumSet<EventFlag> enumSet, int rssi) {
                    getDataWriter().addDataPoint(DataField.PWR_RSSI, estTimestamp, rssi);
                }
            });

            antPluginPcc.subscribeCrankParametersEvent(new AntPlusBikePowerPcc.ICrankParametersReceiver() {
                @Override
                public void onNewCrankParameters(
                        final long estTimestamp, final EnumSet<EventFlag> eventFlags,
                        final AntPlusBikePowerPcc.CrankParameters crankParameters) {
                    getDataWriter().addDataPoint(DataField.PWR_CRANK_LENGTH_STATUS, estTimestamp, crankParameters.getCrankLengthStatus().toString());
                    getDataWriter().addDataPoint(DataField.PWR_SensorSoftwareMismatchStatus, estTimestamp, crankParameters.getSensorSoftwareMismatchStatus().toString());
                    getDataWriter().addDataPoint(DataField.PWR_SensorAvailabilityStatus, estTimestamp, crankParameters.getSensorAvailabilityStatus().toString());
                    getDataWriter().addDataPoint(DataField.PWR_CustomCalibrationStatus, estTimestamp, crankParameters.getCustomCalibrationStatus().toString());
                    getDataWriter().addDataPoint(DataField.PWR_AutoCrankLengthSupport, estTimestamp, crankParameters.isAutoCrankLengthSupported());
                    getDataWriter().addDataPoint(DataField.PWR_CrankLengthStatus, estTimestamp, crankParameters.getFullCrankLength().toString());
                }
            });

            antPluginPcc.subscribeManufacturerIdentificationEvent(new AntPlusCommonPcc.IManufacturerIdentificationReceiver() {
                        @Override
                        public void onNewManufacturerIdentification(final long estTimestamp,
                                                                    final EnumSet<EventFlag> eventFlags, final int hardwareRevision,
                                                                    final int manufacturerID, final int modelNumber) {
                            getDataWriter().addDataPoint(DataField.PWR_HARDWARE_REV, estTimestamp, hardwareRevision);
                            getDataWriter().addDataPoint(DataField.PWR_MANU_ID, estTimestamp, manufacturerID);
                            getDataWriter().addDataPoint(DataField.PWR_MODEL_NUM, estTimestamp, modelNumber);
                        }
                    });

            antPluginPcc.subscribeProductInformationEvent(new AntPlusCommonPcc.IProductInformationReceiver() {
                @Override
                public void onNewProductInformation(final long estTimestamp,
                                                    final EnumSet<EventFlag> eventFlags, final int mainSoftwareRevision,
                                                    final int supplementalSoftwareRevision, final long serialNumber) {

                    getDataWriter().addDataPoint(DataField.PWR_MAIN_SOFTWARE_REV, estTimestamp, mainSoftwareRevision);
                    getDataWriter().addDataPoint(DataField.PWR_SUPPL_SOFTWARE_REV, estTimestamp, supplementalSoftwareRevision);
                    getDataWriter().addDataPoint(DataField.PWR_SERIAL_NUM, estTimestamp, serialNumber);
                }
            });

            antPluginPcc.subscribeBatteryStatusEvent(
                    new AntPlusCommonPcc.IBatteryStatusReceiver() {
                        @Override
                        public void onNewBatteryStatus(final long estTimestamp,
                                                       EnumSet<EventFlag> eventFlags, final long cumulativeOperatingTime,
                                                       final BigDecimal batteryVoltage, final BatteryStatus batteryStatus,
                                                       final int cumulativeOperatingTimeResolution, final int numberOfBatteries,
                                                       final int batteryIdentifier) {
                            getDataWriter().addDataPoint(DataField.PWR_CUM_OPERATING_TIME, estTimestamp, cumulativeOperatingTime);
                            getDataWriter().addDataPoint(DataField.PWR_BATT_VOLTAGE, estTimestamp, batteryVoltage);
                            getDataWriter().addDataPoint(DataField.PWR_BATT_STATUS, estTimestamp, batteryStatus.toString());
                            getDataWriter().addDataPoint(DataField.PWR_CUM_OPER_TIME_RESOLUTION, estTimestamp, cumulativeOperatingTimeResolution);
                            getDataWriter().addDataPoint(DataField.PWR_NUMBER_OF_BATTERIES, estTimestamp, numberOfBatteries);
                            getDataWriter().addDataPoint(DataField.PWR_BATT_IDENTIFIER, estTimestamp, batteryIdentifier);
                        }
                    });
    }

    public void requestCalibration() {
        /*Log.i(TAG, "Requesting manual calibration for power meter");
        boolean submitted = pwrPcc.requestManualCalibration(requestFinishedReceiver);
        if(!submitted) {
            Log.i(TAG, "Request for manual calibration was not sent successfully");
        } else {
            Log.i(TAG, "Request for manual calibration was sent successfully to power meter");
        }*/
        Log.i(getName(), "Requesting CUSTOM calibration for power meter");
        //byte[] customParameters = new byte[]{0, 4, 0, 0, 0, 0};
        //antPluginPcc.requestCustomCalibrationParameters(customParameters, );

    }

    public void requestCollection() {

        /*boolean submitted = pwrPcc.requestSetAutoZero(true, requestFinishedReceiver);
        submitted = pwrPcc.requestCrankParameters(requestFinishedReceiver);
        submitted = pwrPcc.requestSetCrankParameters(AntPlusBikePowerPcc.CrankLengthSetting.AUTO_CRANK_LENGTH, null, requestFinishedReceiver);
        AntPlusBikePowerPcc.CrankLengthSetting newSetting = AntPlusBikePowerPcc.CrankLengthSetting.MANUAL_CRANK_LENGTH;
        //TODO UI to allow user to input crank length
        BigDecimal newCrankLength = new BigDecimal("175.0");
        submitted = pwrPcc.requestSetCrankParameters(newSetting, newCrankLength, requestFinishedReceiver);

        byte[] customParameters = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
        submitted = pwrPcc.requestCustomCalibrationParameters(customParameters, requestFinishedReceiver);

        customParameters = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
        submitted = pwrPcc.requestSetCustomCalibrationParameters(customParameters, requestFinishedReceiver);
        BigDecimal newSlope = new BigDecimal("10.0");
        submitted = pwrPcc.requestSetCtfSlope(newSlope,  requestFinishedReceiver);

        int exampleCommandId = 42;
        byte[] exampleCommandData = new byte[8];
        submitted = pwrPcc.requestCommandBurst(exampleCommandId, exampleCommandData, requestFinishedReceiver);*/
    }

}
