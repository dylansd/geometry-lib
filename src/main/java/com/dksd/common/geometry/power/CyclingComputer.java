package com.dksd.simplecycle.simplecycle;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.icu.text.DecimalFormat;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.INTERNET;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class CyclingComputer extends AppCompatActivity implements LocationListener {

    private TextView speedTextView;
    private TextView powerTextView;
    private static TextView slopeTextView;
    private LocationManager locationManager;
    private Deque<Location> gpsLocations = new ConcurrentLinkedDeque<>();
    private AtomicLong lastUpdate = new AtomicLong(0);
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private static final int LED_NOTIFICATION_ID= 0; //arbitrary constant
    private static double driveEfficiency = 0.96;

    private volatile double slope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cycling_computer);
        speedTextView = (TextView) findViewById(R.id.speed);
        powerTextView = (TextView) findViewById(R.id.power);
        slopeTextView = (TextView) findViewById(R.id.slope);

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        } catch (Throwable ep) {
            Log.e("LOCA", "LocationFeatue ctor getting service context error: ", ep);
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long now = System.currentTimeMillis();
                if (now - lastUpdate.get() > 15 * 1000) {
                    speedTextView.setText("--");
                    powerTextView.setText("--");
                    slopeTextView.setText("--");
                    lastUpdate.set(now);
                }
            }
        };
        executorService.scheduleWithFixedDelay(runnable, 5, 5, TimeUnit.SECONDS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("LApp", "Permission granted for request: " + requestCode);
                } else {
                    Log.i("LApp", "Permission denied for request: " + requestCode);
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LAPP", "Registering intent receivers...");
        registerForUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        deregisterFromUpdates();
    }

    public void registerForUpdates() {
        Log.i("LOCA", "Registering for location updates!");
        final LocationListener locationListener = this;
        Activity act = this;
        Runnable registerRunnable = new Runnable() {

            @Override
            public void run() {
                try {
                    Log.i("LOCA", "Running the register for location updates runnable!");
                    if (checkSelfPermission(INTERNET) != PERMISSION_GRANTED
                            || checkSelfPermission(ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                            || checkSelfPermission(ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
                        Log.e("LOCA", "Could not get permissions in the service so fail!");
                        String[] arr = {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION, INTERNET};
                        ActivityCompat.requestPermissions(act,
                                arr,
                                1);
                    }
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            1000l, 3f, locationListener);
                } catch (Throwable wp) {
                    Log.e("LOCA", "Error in registering for location updates", wp);
                }
            }
        };
        runOnLooperThread("Register Location Updates", registerRunnable);
    }

    public void deregisterFromUpdates() {
        Log.i("LOCA", "Deregistering from updates.");
        final LocationListener locationListener = this;
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    Log.i("LOCA", "Running the deregister for location updates runnable!");
                    if (checkSelfPermission(INTERNET) != PERMISSION_GRANTED
                            || checkSelfPermission(ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                            || checkSelfPermission(ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
                        Log.e("LOCA", "Could not get permissions in the service so fail!");
                        return;
                    }
                    locationManager.removeUpdates(locationListener);
                } catch (Throwable wp) {
                    Log.e("LOCA", "Error in deregistering for location updates", wp);
                }
            }
        };
        runOnLooperThread("De-register Location Updates", runnable);
    }

    public void runOnLooperThread(String threadName, Runnable runnable) {
        try {
            HandlerThread hThread = new HandlerThread(threadName);
            hThread.start();
            final Handler handler = new Handler(hThread.getLooper());
            handler.post(runnable);
        } catch (Throwable tt) {
            Log.e("LOCA", "Caught funky exception: " + tt);
        }
    }

    public void onProviderDisabled(final String provider) {
        try {
            Log.i("LOCA", "Gps Disabled");
        } catch (Throwable ep) {
            Log.e("LOCA", "GPS disabled error!", ep);
        }
    }

    public void onProviderEnabled(final String provider) {
        try {
            Log.i("LOCA", "Gps Enabled");
        } catch (Throwable ep) {
            Log.e("LOCA", "GPS disabled error!", ep);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location == null) {
                return;
            }
            long now = System.currentTimeMillis();
            //if (location.getSpeed() >= 1.5) {
                gpsLocations.add(location);
                calcSlope();
                double power = calcPower(location.getSpeed(), 0.36, 90, slope);
                updateField(location.getSpeed() * 3.6, speedTextView, "km/h");
                updateField(power, powerTextView, "W");
                updateField(slope * 100, slopeTextView, "%", true);
                lastUpdate.set(now);

                if (gpsLocations.getFirst().distanceTo(location) > 20) {
                    gpsLocations.removeFirst();
                }
                if (gpsLocations.getFirst().distanceTo(location) > 20) {
                    gpsLocations.removeFirst();
                }
            //}
        } catch (Throwable ep) {
            Log.e("LOCA", "Error when handling onLocationChanged!", ep);
        }
    }

    private void calcSlope() {
        Location last = gpsLocations.getLast();
        Location first = gpsLocations.getFirst();
        double dist = first.distanceTo(last);
        if (dist < 10) {
            Log.i("LOCA", "Distance between first and last too small: " + dist);
            return;
        }
        double rise = first.getAltitude() - last.getAltitude();
        slope = rise / dist;
    }

    public void onStatusChanged(final String provider, final int status, final Bundle extras) {
        //Log.i("LOCA", "Gps status changed: " + provider + " status: " + status);
    }

/*
    public static double calcPower(double vms, double cda, double totalMass, double slope) {
        return (getPowerDrag(vms, cda)
                + getPowerRolling(vms, totalMass)
                + getPowerClimbing(vms, totalMass, slope)) / driveEfficiency;
    }

    private static double getPowerRolling(double vms, double riderMass) {
        return vms * riderMass * 9.8 * 0.005;
    }

    private static double getPowerClimbing(double vms, double riderMass, double slope) {
        return vms * riderMass * 9.8 * slope;
    }

    private static double getPowerDrag(double vms, double cda) {
        return 0.5*vms*vms*vms*cda;
    }
*/
    private void updateField(double value, TextView textView, String suffix, boolean decimals) {
        String pattern = "####";
        if (decimals) {
            pattern = "####.#";
        }
        textView.setText(fNumber(suffix, value, pattern));
    }

    private void updateField(double value, TextView textView, String suffix) {
        updateField(value, textView, suffix, false);
    }

    private String fNumber(String suffix, double value, String pattern) {
        return customFormat(pattern, value) + " " + suffix;
    }

    public String customFormat(String pattern, double value) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        return myFormatter.format(value);
    }
}
