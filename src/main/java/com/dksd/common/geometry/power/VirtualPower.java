package com.cycling.science.trainwithscience.training;

import com.neuralnetwork.Network;
import com.pso.FitnessFunction;
import com.pso.Gene;
import com.pso.Swarm;
import com.pso.SwarmImpl;

/**
 * Created by dscottdawkins on 5/20/17.
 */

public class VirtualPower {

    public static Network elevationSmoother = new Network(4, 15, 1);
    static double[] inputs = new double[4];

    static double periods = 1;

    /*public static double smoothElevation(final double[] xvals, final double []yvals, final double startSlope, final double endSlope) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                double prevPower = 0;
                double error = 0;
                for (int i = 1; i < gene.size()-1; i++) {
                    double power = calcPower(getVel(i), 0.4, 90, getSlope(i));
                    double diff = Math.abs(power - prevPower);
                    if (diff > 3 && Math.abs(getVel(i) - getVel(i - 1)) <= 2) {//greater than 3 watts and hardly any speed change
                        error += Math.abs(power - prevPower);
                    }
                    prevPower = power;
                    error += Math.abs(getElevation(i) - gene.getValue(i));
                }
                return error;
            }

            @Override
            public int getDimension() {
                return elevationSmoother.getWeightLength();
            }

            @Override
            public void defineDomain(double [] domain) {
                for (int i = 0 ; i < getDimension(); i++) {
                    domain[i] = 200;
                }
            }
        }, 10, false);
        swarm.runUntilConverged(500);
        return swarm.getGbest().getValue(0);
    }*/

    /**
     * Normalized elevation data... between 0,1
     * and the distance between points is the same but arbitrary
     * @param rawElev
     * @return
     */
    public static double smoothElevation(final double rawElev[]) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                double error = 0;
                double prevSlope = rawElev[0];
                for (int i = 0; i < rawElev.length; i++) {
                    double newElevation = prevSlope + gene.getValue(i);
                    error += Math.abs(rawElev[i] - newElevation);
                    prevSlope = newElevation;
                }
                return error;
            }

            @Override
            public int getDimension() {
                return rawElev.length;
            }

            @Override
            public void defineDomain(double [] domain) {
                for (int i = 0 ; i < getDimension(); i++) {
                    domain[i] = 2; //max slope change allowed, must support negative too
                }
            }
        }, 10, false);
        swarm.runUntilConverged(500);
        return swarm.getGbest().getValue(0);
    }

    public static double calcVelocity(final double power, final double cda, final double totalMass, final double slope) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                return Math.abs(power - calcPower(gene.getValue(0), cda, totalMass, slope));
            }

            @Override
            public int getDimension() {
                return 1;
            }

            @Override
            public void defineDomain(double [] domain) {
                domain[0] = 50; //m/s
            }
        }, 5, false);
        swarm.runUntilConverged(5);
        return swarm.getGbest().getValue(0);
    }

    public static double calcSlope(final double power, final double vms, final double cda, final double totalMass) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                return Math.abs(power - calcPower(vms, cda, totalMass, gene.getValue(0)));
            }

            @Override
            public int getDimension() {
                return 1;
            }

            @Override
            public void defineDomain(double [] domain) {
                domain[0] = 0.3; //gradient
            }
        }, 5, false);
        swarm.runUntilConverged(5);
        return swarm.getGbest().getValue(0);
    }

    public static double calcMass(final double power, final double vms, final double cda, final double slope) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                return Math.abs(power - calcPower(vms, cda, gene.getValue(0), slope));
            }

            @Override
            public int getDimension() {
                return 1;
            }

            @Override
            public void defineDomain(double [] domain) {
                domain[0] = 150; //total mass
            }
        }, 5, false);
        swarm.runUntilConverged(5);
        return swarm.getGbest().getValue(0);
    }

    public static double calcCDA(final double power, final double vms, final double totalMass, final double slope) {
        Swarm swarm = new SwarmImpl(new FitnessFunction() {
            @Override
            public double calcFitness(Gene gene) {
                //double vms, double cda, double totalMass, double slope
                return Math.abs(power - calcPower(vms, gene.getValue(0), totalMass, slope));
            }

            @Override
            public int getDimension() {
                return 1;
            }

            @Override
            public void defineDomain(double [] domain) {
                domain[0] = 1; //CDA should be about 0.3
            }
        }, 5, false);
        swarm.runUntilConverged(5);
        return swarm.getGbest().getValue(0);
    }

}
