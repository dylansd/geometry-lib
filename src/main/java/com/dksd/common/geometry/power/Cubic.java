package com.dksd.gpxloader.track;

public class Cubic {
    private final double a;
    private final double b;
    private final double c;
    private final double d;
    private final double fitness;
    private final int startIndex;
    private final int endIndex;

    public Cubic(double a, double b, double c, double d, double fitness, int startIndex, int endIndex) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.fitness = fitness;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public double getEndSlope() {
        return 3 * a + 2 * b + c;
    }

    public double getStartSlope() {
        return c;
    }

    public double getFitness() {
        return fitness;
    }

    @Override
    public String toString() {
        return "Cubic{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", d=" + d +
                ", fitness=" + fitness +
                ", start=" + startIndex +
                ", end=" + endIndex +
                '}';
    }

    public int getEnd() {
        return endIndex;
    }

    public int getStart() {
        return startIndex;
    }
}
