package com.cycling.science.trainwithscience.feature;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.cycling.science.trainwithscience.DataField;
import com.cycling.science.trainwithscience.SensorService;
import com.cycling.science.trainwithscience.elevation.SlopeCalculator;
import com.cycling.science.trainwithscience.logging.ALoggerFactory;
import com.cycling.science.trainwithscience.writer.DataWriter;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by dscottdawkins on 5/1/17.
 */

public class LocationFeature extends AbstractAntFeature<AntPluginPcc> implements LocationListener {

    private SensorService sensorService;
    private LocationManager locationManager;
    private AtomicBoolean requestedUpdates = new AtomicBoolean(false);
    private AtomicBoolean motionDetected = new AtomicBoolean(false);
    private SlopeCalculator slopeCalculator;
    private LinkedList<Float> avgSpeedValues = new LinkedList<>();
    private long timeStampMotionStarted = 0;

    public LocationFeature(String name,
                           SensorService sensorService,
                           DataWriter dataWriter,
                           SlopeCalculator slopeCalculator,
                           ALoggerFactory aLoggerFactory) {
        super(name, dataWriter, aLoggerFactory);
        this.sensorService = sensorService;
        this.slopeCalculator = slopeCalculator;
        try {
            locationManager = (LocationManager) sensorService.getSystemService(Context.LOCATION_SERVICE);
        } catch (Throwable ep) {
            Log.e(getName(), "LocationFeatue ctor getting service context error: ", ep);
        }
    }

    @Override
    public void registerForUpdates() {
        Log.i(getName(), "Registering for location updates!");
        final LocationListener locationListener = this;
        Runnable registerRunnable = new Runnable() {

            @Override
            public void run() {
                try {
                    Log.i(getName(), "Running the register for location updates runnable!");
                    if (sensorService.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && sensorService.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Log.i(getName(), "Could not get permissions in the service so fail!");
                    }
                    if (!requestedUpdates.get()) {
                        requestedUpdates.set(true);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000l, 0f, locationListener);
                    }
                } catch (Throwable wp) {
                    Log.e(getName(), "Error in registering for location updates", wp);
                }
            }
        };
        runOnLooperThread("Register Location Updates", registerRunnable);
    }

    @Override
    public void deregisterFromUpdates() {
        Log.i(getName(), "Deregistering from updates.");
        final LocationListener locationListener = this;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(getName(), "Running the deregister for location updates runnable!");
                    if (sensorService.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && sensorService.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Log.i(getName(), "Could not get permissions in the service so fail!");
                    }
                    if (requestedUpdates.get()) {
                        locationManager.removeUpdates(locationListener);
                        requestedUpdates.set(false);
                    }
                } catch (Throwable wp) {
                    Log.e(getName(), "Error in deregistering for location updates", wp);
                }
            }
        };
        runOnLooperThread("De-register Location Updates", runnable);
    }

    @Override
    protected void subscribeToEvents() {

    }

    @Override
    protected void requestAccessToPcc() {

    }

    public void onLocationChanged(final Location location) {
        try {
            if (location == null) {
                return;
            }
            getDataWriter().addDataPoint(DataField.LATITUDE, location.getTime(), location.getLatitude());
            getDataWriter().addDataPoint(DataField.LONGITUDE, location.getTime(), location.getLongitude());
            getDataWriter().addDataPoint(DataField.ALTITUDE, location.getTime(), location.getAltitude());
            getDataWriter().addDataPoint(DataField.BEARING, location.getTime(), location.getBearing());
            getDataWriter().addDataPoint(DataField.SPEED, location.getTime(), location.getSpeed() * 3.6);
            getDataWriter().addDataPoint(DataField.ACCURACY, location.getTime(), location.getAccuracy());
            if (location != null) {//m/s
                avgSpeedValues.addLast(location.getSpeed());
                if (avgSpeedValues.size() > 30) {
                    avgSpeedValues.removeFirst();
                }
            }
            /*DELAY no need to focus on generated values
            String barometer_elevation = getDataWriter().getClosestTimeValue(DataField.BAROMETRIC_ELEVATION_METERS, location.getTime());
            if (barometer_elevation != null) {
                slopeCalculator.update(location.getLatitude(), location.getLongitude(), Double.parseDouble(barometer_elevation), location.getTime());
                try {
                    double slope = slopeCalculator.getSlope();
                    getDataWriter().addDataPoint(SLOPE, location.getTime(), slope);
                    double pwer = VirtualPower.calcPower(location.getSpeed(), location.getSpeed(), 0.4, 95, slope);
                    getDataWriter().addDataPoint(DataField.VIRTUAL_POWER, location.getTime(), pwer);
                } catch (Exception ep) {
                    ep.printStackTrace();
                }
            }*/
        } catch (Throwable ep) {
            Log.e(getName(), "Error when handling onLocationChanged!", ep);
        }
    }

    private float calcAvgSpeed(LinkedList<Float> avgSpeedValues) {
        float total = 0;
        for (Float avgSpeedValue : avgSpeedValues) {
            total += avgSpeedValue;
        }
        return total / (float) avgSpeedValues.size();
    }

    /*private Double calcFromHR(Location location) {
        try {
            double hr = Double.parseDouble(getDataWriter().getLatestValue(DataField.HR_HEART_RATE));
            double estPower = hr * 2.0 - 90.0;
            return VirtualPower.calcSlope(estPower, location.getSpeed(), 0.352, 90);
        } catch (NullPointerException npe) {
            return null;
        }
    }*/

    public void onProviderDisabled(final String provider) {
        try {
            Log.i(getName(), "Gps Disabled");
        } catch (Throwable ep) {
            Log.e(getName(), "GPS disabled error!", ep);
        }
    }

    public void onProviderEnabled(final String provider) {
        try {
            Log.i(getName(), "Gps Enabled");
        } catch (Throwable ep) {
            Log.e(getName(), "GPS disabled error!", ep);
        }
    }

    public void onStatusChanged(final String provider, final int status, final Bundle extras) {
        Log.i(getName(), "Gps status changed: " + provider + " status: " + status);
    }

    public boolean getMotionDetected() {
        if (motionDetected.get() && System.currentTimeMillis() - timeStampMotionStarted > 600000) {
            getLogger().info("Locat", "Long time (10 mins) sitting still?, setting motion to false");
            motionDetected.set(false);
        }
        if (avgSpeedValues.size() >= 20) {
            getLogger().info("Locat", "Recorded entries greater than 20");
            float avgSpd = calcAvgSpeed(avgSpeedValues);
            if (avgSpd >= 2.8) {
                getLogger().info("Locat", "Avg speed greater than 2.8 m/s: " + avgSpd);
                motionDetected.set(true);
                timeStampMotionStarted = System.currentTimeMillis();
            }
        }
        return motionDetected.get();
    }

    public boolean getRequestedUpdates() {
        return requestedUpdates.get();
    }

}
